//
//  RegisterTableViewController.swift
//  UIoutletApp
//
//  Created by Bala Murugan on 8/15/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

import UIKit

class RegisterTableViewController: UITableViewController {
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var textfieldUsername : UITextField!
    @IBOutlet weak var textfieldPassword : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        let imageBG = UIImageView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        imageBG.image = UIImage(named: "IntroBg")
        self.tableView.backgroundView = imageBG
        
        textfieldEmail.setLeftPadding(20)
        textfieldUsername.setLeftPadding(20)
        textfieldPassword.setLeftPadding(20)
        self.tableView.tableFooterView?.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height - 455)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionTerms(sender: AnyObject) {
        let termsVC = self.storyboard?.instantiateViewControllerWithIdentifier("kTermsVC") as! TermsViewController
        let navVC = UINavigationController(rootViewController: termsVC)
        self.presentViewController(navVC, animated: true, completion: nil)
        
    }

    @IBAction func onSignUpButtonPressed (sender : UIButton){
        if textfieldEmail.text == "" || textfieldPassword.text == "" || textfieldUsername.text == ""{
            UIAlertView(title: "Warning", message: "Please enter all the fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }else if !isValidEmail(textfieldEmail.text!){
            UIAlertView(title: "Warning", message: "Please enter the valid email", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            ProgressHUD.show("Processing...")
            User.signUp(textfieldEmail.text!, username: textfieldUsername.text!, password: textfieldPassword.text!, completion: { (success, result, error) in
                if success{
                    ProgressHUD.dismiss()
                    AppDelegate.sharedInstance().didLogin()
                }else{
                    ProgressHUD.showError(error?.localizedDescription)
                }
            })
        }
    }
    
    func isValidEmail (str : String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(str)
    }


}

extension RegisterTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
