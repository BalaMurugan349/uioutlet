//
//  AppDelegate.swift
//  UIoutletApp
//
//  Created by Aswin on 29/09/15.
//  Copyright (c) 2015 Aswin. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
//foo

let currentDevice = UIDevice.currentDevice()
private let iosVersion = NSString(string: currentDevice.systemVersion).doubleValue
let iOS8 = iosVersion >= 8
let iOS7 = iosVersion >= 7 && iosVersion < 8

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIAlertViewDelegate, LoginViewControllerDelegate, SettingsViewControllerDelegate {
    
    var window: UIWindow?
    var pushReceivedDesignID : String?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //        if (iOS8
        //        {
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [UIUserNotificationType.Sound, UIUserNotificationType.Badge, UIUserNotificationType.Alert] , categories: nil))
        UIApplication.sharedApplication().registerForRemoteNotifications()
        //        }
        //        else
        //        {
        //            UIApplication.sharedApplication().registerForRemoteNotificationTypes([UIRemoteNotificationType.Sound, UIRemoteNotificationType.Alert, UIRemoteNotificationType.Badge])
        //        }
        
        
//                        for name in UIFont.familyNames() {
//                            print(name)
////                            if let nameString = name as? String
////                            {
//                                print(UIFont.fontNamesForFamilyName(name))
////                            }
//                        }
        
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        
        UITabBar.appearance().tintColor = UIColor.blackColor()
        
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSFontAttributeName: UIFont.boldSystemFontOfSize(14)],
            forState: .Normal)
        
        _  = FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //        if (FBSDKAccessToken.currentAccessToken() == nil) {
        //
        //        }
        //        else {
        //            print("Logged in...")
        //            User.currentUser().setUserDetails(User.fetchUserDetails()!)
        //
        //            self.setMainView()
        //        }
        
        if User.fetchUserDetails() == nil {
            print("Not logged in...")
            self.setLoginView()
        }
        else {
            print("Logged in...")
            User.currentUser().setUserDetails(User.fetchUserDetails()!)
            
            
            if let launchOpts = launchOptions {
                if let notificationDetails: NSDictionary = launchOpts[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
                    if let notification = notificationDetails["aps"] as? NSDictionary,
                        let designId = notification["design"] as? String {
                            pushReceivedDesignID = designId
                        self.setMainView()
                            self.showDetailDesign(self.pushReceivedDesignID!)
                    }
                }
            }else{
                self.setMainView()
//                self.showDetailDesign("206")

            }
        }
        
        return true
    }
    
    
    func didLogin() {
        self.setMainView()
    }
     class func sharedInstance() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    func didLogout() {
        self.setLoginView()
    }
    
    func setLoginView() {
        let storyBoard : UIStoryboard = self.window!.rootViewController!.storyboard!
        let nav : UINavigationController = storyBoard.instantiateViewControllerWithIdentifier("RootNav") as! UINavigationController
        //let loginViewController: LoginViewController  = storyBoard.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
        //loginViewController.delegate = self
        self.window?.rootViewController = nav
    }
    
    func setMainView() {
        let storyBoard : UIStoryboard = self.window!.rootViewController!.storyboard!
        let mainViewNavigationController : UINavigationController = storyBoard.instantiateViewControllerWithIdentifier("MainViewNC") as! UINavigationController
        self.window!.rootViewController = mainViewNavigationController
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let characterSet = NSCharacterSet(charactersInString: " ><")
        let deviceTockenFiltered = ("\(deviceToken)".componentsSeparatedByCharactersInSet(characterSet) as NSArray).componentsJoinedByString("")
        //        UIAlertView(title: "Push Alert", message: "Device Token" + deviceTockenFiltered, delegate: nil, cancelButtonTitle: "OK").show()
        
        NSUserDefaults.standardUserDefaults().setObject(deviceTockenFiltered, forKey: "kdeviceTocken")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error.localizedDescription, terminator: "")
        
        NSUserDefaults.standardUserDefaults().setObject("f1d532c77f5b21bd7b69b8d7c65f3d379c7bf134cbcf0c5f8d002adc4566f4fa", forKey: "kdeviceTocken")
        NSUserDefaults.standardUserDefaults().synchronize()
        //        UIAlertView(title: "Push Alert", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "OK").show()
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
            if let notification = userInfo["aps"] as? NSDictionary,
                let alert = notification["alert"] as? String {
                if let newDesignId = notification["design"] as? String {
                    pushReceivedDesignID = newDesignId
                    if (UIApplication.sharedApplication().applicationState != UIApplicationState.Active)
                    {
                        self.showDetailDesign(pushReceivedDesignID!)
                    }   else {
                        UIAlertView(title: "Message", message: alert, delegate: self, cancelButtonTitle: "Yes", otherButtonTitles: "Not now").show()
                    }
                }
//        }
        }
    }
    
    
    //MARK: - UIAlertViewDelegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch (buttonIndex){
        case alertView.cancelButtonIndex :
            if ((pushReceivedDesignID!.characters.count) != 0 && !(pushReceivedDesignID!.isEmpty)) {
                self.showDetailDesign(pushReceivedDesignID!)
            }
            break
        default :
            break
        }
    }
    
    func showDetailDesign(designId : String) {
        if User.currentUser().id != nil {
            
            let storyBoard : UIStoryboard = self.window!.rootViewController!.storyboard!
            let mainViewNavigationController : UINavigationController = storyBoard.instantiateViewControllerWithIdentifier("MainViewNC") as! UINavigationController
            if let tabBarController = mainViewNavigationController.viewControllers.first as? UITabBarController {
                let designDetailViewController: DesignDetailViewController  = storyBoard.instantiateViewControllerWithIdentifier("DesignDetailVC") as! DesignDetailViewController
                designDetailViewController.designNewID = designId
                let designDetailNavigationController : UINavigationController = UINavigationController(rootViewController: designDetailViewController)
                tabBarController.viewControllers![0] = designDetailNavigationController
            }
            self.window!.rootViewController = mainViewNavigationController

            
            
            
//            let storyBoard : UIStoryboard = self.window!.rootViewController!.storyboard!
            
            
//            self.window?.rootViewController?.presentViewController(designDetailNavigationController, animated: true, completion: nil)
        }
    }
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        
        print("url received \(url)")
        print("query string \(url.query)")
        print("url host \(url.host)")
        //        print("query string \(url.query)")
        
        if url.host == "share" {
            var designIDShared = url.query
            designIDShared = designIDShared!.substringWithRange(Range<String.Index>(start: designIDShared!.startIndex.advancedBy(3), end: designIDShared!.endIndex))
            print("design id \(designIDShared)")
            self.showDetailDesign(designIDShared!)
        }
        
        if (LISDKCallbackHandler.shouldHandleUrl(url)) {
            return LISDKCallbackHandler.application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        _  = FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.aswin.UIoutletApp" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("UIoutletApp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("UIoutletApp.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    abort()
                }
            }
        }
    }
    
}

