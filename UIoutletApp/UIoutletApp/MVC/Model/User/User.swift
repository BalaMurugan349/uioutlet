//
//  User.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

var user : User?

class User: NSObject {
    
    var id          : String?
    var firstName   : String?
    var lastName    : String?
    var email       : String?
    var facebookId  : String?
    var linkedInId  : String?
    var image       : String?
    var socialType  : String?
    var userName    : String?
    
    
//        {
//            "result": "success",
//            "user": {
//                "id": "7",
//                "first_name": "X",
//                "last_name": "Y",
//                "mail_id": "crab1990@gmail.com",
//                "facebook_id": "1234",
//                "linkedin_id": null
//            }
//    }
    override init()
    {
        super.init()
    }
    
    init(userDetails : NSDictionary)
    {
        super.init()
        self.firstName = userDetails["first_name"] as? String
        self.lastName = userDetails["last_name"] as? String
        self.email = userDetails["mail_id"] as? String
        self.id =  userDetails["id"] as? String
        print("id == \(self.id)")
        
        self.image = userDetails["profile_pic"] as? String
        self.facebookId =  userDetails["facebook_id"] as? String
        self.linkedInId = userDetails["linkedin_id"] as? String
        
        
        self.userName = userDetails["username"] as? String
        self.socialType = "fb"
    }
    
    required convenience init(coder decoder: NSCoder) {
        self.init()
        self.firstName = decoder.decodeObjectForKey("firstName") as? String
        self.lastName = decoder.decodeObjectForKey("lastName") as? String
        self.email = decoder.decodeObjectForKey("email") as? String
        self.id = decoder.decodeObjectForKey("id") as? String
        self.image = decoder.decodeObjectForKey("image") as? String
        self.facebookId =  decoder.decodeObjectForKey("facebookId") as? String
        self.linkedInId = decoder.decodeObjectForKey("linkedInId") as? String
        self.socialType = decoder.decodeObjectForKey("socialType") as? String
        self.userName = decoder.decodeObjectForKey("username") as? String
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.firstName, forKey: "firstName")
        coder.encodeObject(self.lastName, forKey: "lastName")
        coder.encodeObject(self.email, forKey: "email")
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.image, forKey: "image")
        coder.encodeObject(self.facebookId, forKey: "facebookId")
        coder.encodeObject(self.linkedInId, forKey: "linkedInId")
        coder.encodeObject(self.socialType, forKey: "socialType")
        coder.encodeObject(self.userName, forKey: "username")
    }
    
    
    ////// SINGLETON FUNCTION
    class func currentUser() -> User
    {
        if (user == nil)
        {
            user = User()
        }
        return user!
    }
    
    func setUserDetails (user : User) {
        User.currentUser().id = user.id
        User.currentUser().email = user.email
        User.currentUser().firstName = user.firstName
        User.currentUser().lastName = user.lastName
        User.currentUser().image = user.image
        User.currentUser().facebookId = user.facebookId
        User.currentUser().linkedInId = user.linkedInId
        User.currentUser().socialType = user.socialType
        User.currentUser().userName = user.userName
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> User {
        user = User(userDetails: userDetails)
        User.storeUserDetails(user!)
        return user!
    }
    
    class func storeUserDetails(user : User) -> User {
        let userObject = NSKeyedArchiver.archivedDataWithRootObject(user)
        NSUserDefaults.standardUserDefaults().setObject(userObject, forKey: "UserDetails")
        NSUserDefaults.standardUserDefaults().synchronize()
        return user
    }
    
    
    class func fetchUserDetails() -> User? {
        if NSUserDefaults.standardUserDefaults().objectForKey("UserDetails") != nil {
            let data = NSUserDefaults.standardUserDefaults().objectForKey("UserDetails") as! NSData
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? User
        } else {
            return nil
        }
    }
    
    class func deleteUserDetails() {
        user = nil
        NSUserDefaults.standardUserDefaults().removeObjectForKey("UserDetails")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("kProfilePic")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    
    class func loginUser(email: String?, firstName: String?, lastName: String?, socialType: String?, socialId: String?, profilePicture: String?, deviceToken: String?, completion:(Bool, AnyObject?, NSError?) -> Void) {
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(email!, key: "mail_id")
        body.addValue(firstName!, key: "first_name")
        body.addValue(lastName!, key: "last_name")
        body.addValue(socialType!, key: "social_type")
        body.addValue(socialId!, key: "social_id")
        body.addValue(profilePicture!, key: "profile_pic")
        if let token: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("kdeviceTocken") {
            body.addValue("\(token)", key: "device_token")
        }
//        body.addValue(deviceToken!, key: "device_token")
        
        if let profilePic = profilePicture {
            NSUserDefaults.standardUserDefaults().setObject(profilePic, forKey: "kProfilePic")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        ServiceManager.fetchDataFromService("user/login", parameters: body) { (success, result, error) -> Void in
            
            print("success \(success) result \(result) error \(error)")
            if success && result != nil && result!["result"] as! String == "success"{
                let userDetails = result!["user"] as! NSDictionary
                completion(true, self.userWithDetails(userDetails), nil)
            }else{
                
                if success && result != nil && result!["result"] as! String == "error" {
                    completion(false, result!["message"], nil)
                }
                else {
                completion(false , nil , error)
                }
            }
        }
    }
    
    class func signUp(email: String?, username: String?,password: String?, completion:(Bool, AnyObject?, NSError?) -> Void) {
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(email!, key: "email")
        body.addValue(username!, key: "username")
        body.addValue(password!, key: "password")
        var deviceToken : String = ""
        
        if let token: String = NSUserDefaults.standardUserDefaults().objectForKey("kdeviceTocken") as? String{
            body.addValue("\(token)", key: "devicetoken")
            deviceToken = token
        }
        else {
            body.addValue("123123h13hu12g3u123u", key: "devicetoken")
            deviceToken = "123123h13hu12g3u123u"
        }
        
        let parameters = "email=\(email!)&username=\(username!)&password=\(password!)&devicetoken=\(deviceToken)"
        
        ServiceManager.fetchDataFromService("login/registration?\(parameters)", parameters: body) { (success, result, error) -> Void in
            print("success \(success) result \(result) error \(error)")
            if success && result != nil && result!["result"] as! String == "Success"{
                let userDetails = result!["user"] as! NSDictionary
                completion(true, self.userWithDetails(userDetails), nil)
            }else{
                
                if success && result != nil && result!["result"] as! String == "Failed" {
                    completion(false,nil , NSError(domain: "", code: 101, userInfo: [NSLocalizedDescriptionKey : result!["message"] as! String]))
                }
                else {
                    completion(false , nil , error)
                }
            }
        }
    }

    class func loginWithEmail(email: String?,password: String?, completion:(Bool, AnyObject?, NSError?) -> Void) {
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(email!, key: "email")
        body.addValue(password!, key: "password")
        
        var deviceToken : String = ""
        
        if let token: String = NSUserDefaults.standardUserDefaults().objectForKey("kdeviceTocken") as? String{
            body.addValue("\(token)", key: "devicetoken")
            deviceToken = token
        }
        else {
            body.addValue("123123h13hu12g3u123u", key: "devicetoken")
            deviceToken = "123123h13hu12g3u123u"
        }
        
        let parameters = "email=\(email!)&password=\(password!)&devicetoken=\(deviceToken)"
        ServiceManager.fetchDataFromService("login/login?\(parameters)", parameters: body) { (success, result, error) -> Void in
            
            print("success \(success) result \(result) error \(error)")
            if success && result != nil && result!["result"] as! String == "Success"{
                let userDetails = result!["user"] as! NSDictionary
                completion(true, self.userWithDetails(userDetails), nil)
            }else{
                
                if success && result != nil && result!["result"] as! String == "Failed" {
                    completion(false,nil , NSError(domain: "", code: 101, userInfo: [NSLocalizedDescriptionKey : result!["message"] as! String]))
                }
                else {
                    completion(false , nil , error)
                }
            }
        }
    }

    func updateUserMail(emailUpdated :String?, completion:(Bool, AnyObject?, NSError?) -> Void) {
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(self.id!, key: "user")
        body.addValue(emailUpdated!, key: "email")
        ServiceManager.fetchDataFromService("user/update", parameters: body) { (success, result, error) -> Void in
            
            print("success \(success) result \(result) error \(error)")
            if success && result != nil && result!["status"] as! String == "success"{
                if result!["message"] as! String == "Updated successfully!" {
                    self.email = emailUpdated
                    NSUserDefaults.standardUserDefaults().removeObjectForKey("UserDetails")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    let userObject = NSKeyedArchiver.archivedDataWithRootObject(self)
                    NSUserDefaults.standardUserDefaults().setObject(userObject, forKey: "UserDetails")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    completion(true, "Profile updated Successfully", nil)
                }
                else {
                    completion(false, result!["message"] as! String, nil)
                }
                
            }else{
                
                if success && result != nil && result!["status"] as! String == "failed"{
                    completion(false, result!["message"], nil)
                }
                else {
                    completion(false , nil , error)
                }
            }
            
        }
    }

    
    class func makeFileName() -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue = arc4random() % 1000
        let fileName = String(format: "%@%d", dateString, randomValue)
        return fileName
    }
    
}