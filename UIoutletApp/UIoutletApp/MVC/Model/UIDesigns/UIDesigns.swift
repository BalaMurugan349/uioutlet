//
//  UIDesigns.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class UIDesigns: NSObject {
    
    var designId : String?
    var designName : String?
    var designDescription : String?
    var designDate : String?
    var designThumbnail : String?
    var designAuthorName : String?
    var designAuthorWebURL : String?
    var designPsdFile : String?
    var designSketchFile : String?
    var designIsFavorite : String?
    var designCreatedDate : String?
    var designUpdatedDate : String?
    var designGalleryList : AnyObject?
    var designGalleryVideoList : AnyObject?
    
    override init() {
        super.init()
    }
    
    init(designDetails : NSDictionary) {
        super.init()
        self.designId = designDetails["id"] as? String
        self.designName = designDetails["name"] as? String
        self.designDescription = designDetails["description"] as? String
        self.designDate = designDetails["date"] as? String
        self.designThumbnail = designDetails["preview_image"] as? String
        self.designAuthorName = designDetails["author_name"] as? String
        self.designAuthorWebURL = designDetails["author_web_link"] as? String
        self.designPsdFile = designDetails["psd_file"] as? String
        self.designSketchFile = designDetails["sketch_file"] as? String
        self.designIsFavorite = designDetails["is_favourite"] as? String
        if let _: String =  designDetails["created_at"] as? String{
            self.designCreatedDate = designDetails["created_at"] as? String
        }
        if let _: String =  designDetails["updated_at"] as? String{
            self.designUpdatedDate = designDetails["updated_at"] as? String
        }
        
        if let _: String =  designDetails["gallery_images"] as? String{
            let galleryImages : String = designDetails["gallery_images"] as! String
            self.designGalleryList = galleryImages.characters.split{$0 == ","}.map(String.init)
        }
        
        if let _: String =  designDetails["gallery_videos"] as? String{
            let galleryVideos : String = designDetails["gallery_videos"] as! String
            self.designGalleryVideoList = galleryVideos.characters.split{$0 == ","}.map(String.init)
        }
    }

    class func getDesignListForUser(userID : String?, type: String?, completion : (success :Bool, result :AnyObject?, error :NSError?) -> Void) {
        
        let serviceRequest : String? = "documents/design/"
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        
        
        if userID == nil {
            completion(success: false, result: nil, error: nil)
            return
        }
        
        body.addValue(userID!, key: "user")
        if type != nil {
            body.addValue(type!, key: "type")
        }
        
        
        ServiceManager.fetchDataFromService(serviceRequest!, parameters: body) { (success, result, error) -> Void in
//            print("resul  = \(result)")
            if success && result != nil && result!["result"] as! String == "success" {
                if (result!["designs"] as AnyObject!).isKindOfClass(NSArray) {
                let designs = result!["designs"] as! NSArray
                let arrayDesigns : NSMutableArray? = NSMutableArray()
                
                designs.enumerateObjectsUsingBlock({ (designDetail , index, stop) -> Void in
//                    print("design  = \(designDetail)")
                    let design : UIDesigns! = UIDesigns(designDetails: designDetail as! NSDictionary)
                    arrayDesigns?.addObject(design!)
                })
                completion(success: true, result: arrayDesigns, error: nil)
                }
                else {
                    completion(success: true, result: nil, error: nil)
                }
            }else{
                if (success && result!["result"] as! String == "failed") {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }
    }
    
    class func getLatestDesignListForUser(userID : String?, type: String?, latestDesignId : String?, completion : (success :Bool, result :AnyObject?, error :NSError?) -> Void) {
        
        let serviceRequest : String? = "documents/design/"
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(userID!, key: "user")
//        body.addValue(latestDesignId!, key: "latest")
         body.addValue(latestDesignId!, key: "latest")
        if type != nil {
            body.addValue(type!, key: "type")
        }
        
        ServiceManager.fetchDataFromService(serviceRequest!, parameters: body) { (success, result, error) -> Void in
            print("resul  = \(result)")
            if success && result != nil && result!["result"] as! String == "success" && (result!["designs"] as AnyObject!).isKindOfClass(NSArray){
                let designs = result!["designs"] as! NSArray
                let arrayDesigns : NSMutableArray? = NSMutableArray()
                
                designs.enumerateObjectsUsingBlock({ (designDetail , index, stop) -> Void in
//                    print("design  = \(designDetail)")
                    let design : UIDesigns! = UIDesigns(designDetails: designDetail as! NSDictionary)
                    arrayDesigns?.addObject(design!)
                })
                completion(success: true, result: arrayDesigns, error: nil)
            }else{
                if success && result!["result"] as! String == "failed" {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }
    }
    
    class func getLastDesignListForUser(userID : String?, type: String?, latestDesignId : String?, completion : (success :Bool, result :AnyObject?, error :NSError?) -> Void) {
        
        let serviceRequest : String? = "documents/design/"
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(userID!, key: "user")
        //        body.addValue(latestDesignId!, key: "latest")
        body.addValue(latestDesignId!, key: "last")
        if type != nil {
            body.addValue(type!, key: "type")
        }
        
        ServiceManager.fetchDataFromService(serviceRequest!, parameters: body) { (success, result, error) -> Void in
            //print("resul  = \(result)")
            if success && result != nil && result!["result"] as! String == "success" && (result!["designs"] as AnyObject!).isKindOfClass(NSArray){
                let designs = result!["designs"] as! NSArray
                if result!["designs"] is NSNull {
                    completion(success: false, result: "Loading Completed", error: nil)
                    return
                }
                let arrayDesigns : NSMutableArray? = NSMutableArray()
                
                designs.enumerateObjectsUsingBlock({ (designDetail , index, stop) -> Void in
                   // print("design  = \(designDetail)")
                    let design : UIDesigns! = UIDesigns(designDetails: designDetail as! NSDictionary)
                    arrayDesigns?.addObject(design!)
                })
                completion(success: true, result: arrayDesigns, error: nil)
            }else{
                if success && result!["result"] as! String == "failed" {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }
    }
    
    
    class func getDesignDetail(requiredDesignId : String?, completion : (success : Bool, result : AnyObject?, error : NSError?) -> Void){
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(User.currentUser().id!, key: "user")
        body.addValue(requiredDesignId!, key: "design")
        
        ServiceManager.fetchDataFromService("documents/share/design/", parameters: body) { (success, result, error) -> Void in
            print("result  = \(result)")
            if success && result != nil && result!["result"] as! String == "success"{
                
                let design : UIDesigns! = UIDesigns(designDetails: result!["design"] as! NSDictionary)
                completion(success: true, result: design, error: nil)
                
            }else{
                if success && result!["result"] as! String == "failed" {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }

    }
    
    func addDesignFavoriteForUser(userId : String?, service : String?, completion : (success : Bool, result : AnyObject?, error : NSError?) -> Void){
        let serviceRequest : String? = "favourite/" + service!
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(userId!, key: "user")
        body.addValue(self.designId!, key: "design")
        
        ServiceManager.fetchDataFromService(serviceRequest!, parameters: body) { (success, result, error) -> Void in
            print("resul  = \(result)")
            if success && result != nil && result!["result"] as! String == "success"{
                completion(success: true, result: "Successful", error: nil)
            }else{
                if success && result!["result"] as! String == "failed" {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }

    }
    
    
    
    func downloadDesignForUser(userId : String?, completion : (success : Bool, result : AnyObject?, error : NSError?) -> Void){
        let serviceRequest : String? = "documents/download/"
        let body : NSMutableData = NSMutableData.postData() as! NSMutableData
        body.addValue(userId!, key: "user")
        body.addValue(self.designId!, key: "design")
        
        ServiceManager.fetchDataFromService(serviceRequest!, parameters: body) { (success, result, error) -> Void in
            print("resul  = \(result)")
            if success && result != nil && result!["result"] as! String == "success"{
                completion(success: true, result: result!["message"], error: nil)
            }else{
                if success && result == nil {
                    completion(success: false, result: "Unknown error occured", error: nil)
                }
                else if success && result!["result"] as! String == "failed" {
                    completion(success: false, result: result!["message"], error: nil)
                }
                else {
                    completion(success: false, result: nil, error: error)
                }
                
            }
        }
        
    }

}
//{
//    "id": "6",
//    "name": "UX Researcher / UI Design Product Design",
//    "is_favourite": null,
//    "is_purchased": null,
//    "description": "UX Researcher / UI Design Product Design Description here",
//    "date": "1443555500",
//    "preview_image": "robberimages3.jpeg",
//    "author_name": "Ritwik (Rick) Sen",
//    "author_web_link": "https://www.behance.net/Rsen",
//    "psd_file": "jasonux2.sql",
//    "scketch_file": "jasonux3.sql",
//    "zip_file": "Document.zip",
//    "gallery_images": "[\"robberimages4.jpeg\",\"speak-not-that-provokes-quarrel1.jpg\"]"
//},