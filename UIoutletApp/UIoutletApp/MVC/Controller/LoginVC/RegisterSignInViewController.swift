//
//  RegisterSignInViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 20/08/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

import UIKit

class RegisterSignInViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var textfieldRegisterEmail : UITextField!
    @IBOutlet weak var textfieldRegisterUsername : UITextField!
    @IBOutlet weak var textfieldRegisterPassword : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var textfieldPassword : UITextField!
    
    @IBOutlet weak var scrollViewLogin: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var constraintContentVerticalScroll: NSLayoutConstraint!
    @IBOutlet weak var constraintSocialVerticalScroll: NSLayoutConstraint!
    @IBOutlet weak var constraintContentHeightScroll: NSLayoutConstraint!
    @IBOutlet weak var constraintContentWidthScroll: NSLayoutConstraint!
    @IBOutlet weak var pageControlLogin: UIPageControl!
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak private var viewLogin: UIView!
    @IBOutlet weak private var viewSocial: UIView!
    @IBOutlet weak var viewContainer : UIView!
    var delegate: LoginViewControllerDelegate?
    var loginPage : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textfieldRegisterEmail.setLeftPadding(20)
        textfieldRegisterUsername.setLeftPadding(20)
        textfieldRegisterPassword.setLeftPadding(20)
        textfieldEmail.setLeftPadding(20)
        textfieldPassword.setLeftPadding(20)
        viewContainer.layoutIfNeeded()
        loginPage == false ? viewContainer.addSubview(viewRegister) : viewContainer.addSubview(viewLogin)
        viewContainer.addSubview(viewSocial)
        self.navigationController?.navigationBarHidden = true
        scrollViewLogin.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width * 2, 0)
        self.pageControlLogin.currentPage = 1
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        constraintContentWidthScroll.constant = 2 * CGRectGetWidth(UIScreen.mainScreen().bounds);
        constraintContentHeightScroll.constant = UIScreen.mainScreen().bounds.height - 93// - 42//CGRectGetHeight(scrollViewLogin.frame);
        if UIScreen.mainScreen().bounds.height > 580 {
//            constraintContentVerticalScroll.constant = 80
//            constraintSocialVerticalScroll.constant = 35
        }
        else {
//            constraintSocialVerticalScroll.constant = 82
        }
        
        self.view.needsUpdateConstraints()
//        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
//        viewRegister.frame = CGRectMake(UIScreen.mainScreen().bounds.width, 0, UIScreen.mainScreen().bounds.width, viewLogin.bounds.height)
//        viewSocial.frame = CGRectMake(CGRectGetMaxX(viewRegister.frame), 0, UIScreen.mainScreen().bounds.width, viewLogin.bounds.height)
        viewSocial.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, viewContainer.bounds.height)
        if loginPage == false{
        viewRegister.frame = CGRectMake(UIScreen.mainScreen().bounds.width, 0, UIScreen.mainScreen().bounds.width, viewContainer.bounds.height)
            scrollViewLogin.setContentOffset(CGPoint(x: UIScreen.mainScreen().bounds.width, y: viewRegister.frame.origin.y), animated: true)

        }else{
            viewLogin.frame = CGRectMake(UIScreen.mainScreen().bounds.width, 0, UIScreen.mainScreen().bounds.width, viewContainer.bounds.height)
            scrollViewLogin.setContentOffset(CGPoint(x: UIScreen.mainScreen().bounds.width, y: viewLogin.frame.origin.y), animated: true)
        }

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        if loginPage == false {
//            scrollViewLogin.setContentOffset(CGPoint(x: viewRegister.frame.origin.x, y: viewRegister.frame.origin.y), animated: true)
//            self.pageControlLogin.currentPage = Int(1)
//        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail (str : String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(str)
    }
    
    
    //MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
       // self.pageControlLogin.currentPage = Int(pageNumber)
        
        if pageNumber == 0 {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    
    //MARK: - ButtonActions
    
    @IBAction func onRegisterButtonPressed (sender : UIButton){
        if textfieldRegisterEmail.text == "" || textfieldRegisterPassword.text == "" || textfieldRegisterUsername.text == ""{
            UIAlertView(title: "Warning", message: "Please enter all the fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }else if !isValidEmail(textfieldRegisterEmail.text!){
            UIAlertView(title: "Warning", message: "Please enter the valid email", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            ProgressHUD.show("Registering...")
            User.signUp(textfieldRegisterEmail.text!, username: textfieldRegisterUsername.text!, password: textfieldRegisterPassword.text!, completion: { (success, result, error) in
                if success{
                    ProgressHUD.dismiss()
                    AppDelegate.sharedInstance().didLogin()
                }else{
                    ProgressHUD.showError(error?.localizedDescription)
                }
            })
        }
    }
    
    
    @IBAction func onSignInButtonPressed (sender : UIButton){
        
        if textfieldEmail.text == "" || textfieldPassword.text == ""{
            UIAlertView(title: "Warning", message: "Please enter all the fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }
            else if !isValidEmail(textfieldEmail.text!){
            UIAlertView(title: "Warning", message: "Please enter the valid email", delegate: nil, cancelButtonTitle: "Ok").show()
        }
            else{
            ProgressHUD.show("Logging in...")
            User.loginWithEmail(textfieldEmail.text!, password: textfieldPassword.text!, completion: { (success, result, error) in
                if success{
                    ProgressHUD.dismiss()
                    AppDelegate.sharedInstance().didLogin()
                }else{
                    ProgressHUD.showError(error?.localizedDescription)
                }
            })
        }
    }

    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionTerms(sender: AnyObject) {
        let termsVC = self.storyboard?.instantiateViewControllerWithIdentifier("kTermsVC") as! TermsViewController
        let navVC = UINavigationController(rootViewController: termsVC)
        self.presentViewController(navVC, animated: true, completion: nil)
        
    }
}

extension RegisterSignInViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        scrollViewLogin.scrollEnabled = false
    }
    func textFieldDidEndEditing(textField: UITextField) {
        scrollViewLogin.scrollEnabled = true
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
