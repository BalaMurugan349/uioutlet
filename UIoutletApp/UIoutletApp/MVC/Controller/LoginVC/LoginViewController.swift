//
//  LoginViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 30/09/15.
//  Copyright (c) 2015 Aswin. All rights reserved.
//

import UIKit
import FBSDKLoginKit

protocol LoginViewControllerDelegate {
   // func didLogin()
}

class LoginViewController: UIViewController, UIScrollViewDelegate {


    @IBOutlet weak var imageViewSplash: UIImageView!
    @IBOutlet weak var scrollViewLogin: UIScrollView!
//    @IBOutlet weak var constraintContentHeightScroll: NSLayoutConstraint!
    @IBOutlet weak var constraintContentWidthScroll: NSLayoutConstraint!
    @IBOutlet weak var pageControlLogin: UIPageControl!
    @IBOutlet weak var viewLoginArea: UIView!
    @IBOutlet weak private var viewHelp: UIView!
    @IBOutlet weak var viewContainer : UIView!
    var delegate: LoginViewControllerDelegate?
//    var isShowingInstruction =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        isShowingInstruction = true
//         self.viewLoginArea.frame = CGRectMake(CGRectGetMaxX(self.viewHelp.frame), CGRectGetMinY(self.viewLoginArea.frame), CGRectGetWidth(self.viewLoginArea.frame), CGRectGetHeight(self.viewLoginArea.frame))
//
//        // Do any additional setup after loading the view.
//        let swipeGestureViewHelp = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipeViewHelp:"))
//        swipeGestureViewHelp.direction = UISwipeGestureRecognizerDirection.Left
////        self.viewSwipeArea .addGestureRecognizer(swipeGestureViewHelp)
//        
//        let swipeGestureViewLoginArea = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipeViewLoginArea:"))
//        swipeGestureViewLoginArea.direction = UISwipeGestureRecognizerDirection.Right
//        self.viewLoginArea .addGestureRecognizer(swipeGestureViewLoginArea)
        viewLoginArea.frame = CGRectMake(UIScreen.mainScreen().bounds.width, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height - 135)
        viewContainer.addSubview(viewLoginArea)
        self.navigationController?.navigationBarHidden = true
        scrollViewLogin.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width * 2, 0)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.view.layoutIfNeeded()
        constraintContentWidthScroll.constant = 2 * CGRectGetWidth(scrollViewLogin.frame);
//        constraintContentHeightScroll.constant = CGRectGetHeight(scrollViewLogin.frame);
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        if self.isShowingInstruction
//        {
//            self.viewLoginArea.frame = CGRectMake(CGRectGetMaxX(self.viewHelp.frame), CGRectGetMinY(self.viewLoginArea.frame), CGRectGetWidth(self.viewLoginArea.frame), CGRectGetHeight(self.viewLoginArea.frame))
//        }
        
        
    }
    
    @IBAction func onRegisterButtonPressed (sender : UIButton){
        let reg : RegisterSignInViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterSignInVC") as! RegisterSignInViewController
        reg.loginPage = false
        self.navigationController?.pushViewController(reg, animated: true)
    }
    
    @IBAction func onSignInButtonPressed (sender : UIButton){
        let sign : RegisterSignInViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterSignInVC") as! RegisterSignInViewController
        sign.loginPage = true
        self.navigationController?.pushViewController(sign, animated: true)

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControlLogin.currentPage = Int(pageNumber)
    }
    
    
    
    //MARK: - GestureRecognizer
//    func handleSwipeViewHelp(sender: UISwipeGestureRecognizer) {
//     
//        UIView.animateWithDuration(0.5, animations: { () -> Void in
//            
//            self.viewHelp.frame = CGRectMake(CGRectGetMinX(self.view.frame) - CGRectGetWidth(self.viewHelp.frame), CGRectGetMinY(self.viewHelp.frame), CGRectGetWidth(self.viewHelp.frame), CGRectGetHeight(self.viewHelp.frame))
//            self.viewLoginArea.frame = CGRectMake(CGRectGetMaxX(self.viewHelp.frame), CGRectGetMinY(self.viewLoginArea.frame), CGRectGetWidth(self.viewLoginArea.frame), CGRectGetHeight(self.viewLoginArea.frame))
//            self.pageControlLogin.currentPage = 2
//            
//            }) { (completed) -> Void in
//                self.viewHelp.hidden = true
//                self.isShowingInstruction = false
//        }
//    }
//    
//    
//    func handleSwipeViewLoginArea(sender: UISwipeGestureRecognizer) {
//        
//        self.viewHelp.hidden = false
//        UIView.animateWithDuration(0.5, animations: { () -> Void in
//            
//            self.viewHelp.frame = CGRectMake(CGRectGetMinX(self.viewLoginArea.frame), CGRectGetMinY(self.viewHelp.frame), CGRectGetWidth(self.viewHelp.frame), CGRectGetHeight(self.viewHelp.frame))
//             self.viewLoginArea.frame = CGRectMake(CGRectGetMaxX(self.viewHelp.frame), CGRectGetMinY(self.viewLoginArea.frame), CGRectGetWidth(self.viewLoginArea.frame), CGRectGetHeight(self.viewLoginArea.frame))
//            self.pageControlLogin.currentPage = 0
//            
//            }) { (completed) -> Void in
//                
//                self.isShowingInstruction = true
//            }
//    }
    
    //MARK: - ButtonActions
    @IBAction func facebookLoginButtonAction(sender: AnyObject) {
        
//        if FBSDKAccessToken.currentAccessToken() != nil {
//            FBSDKLoginManager().logOut()
//            return
//        }
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.imageViewSplash.hidden = false
        }
        
        
        let login:FBSDKLoginManager = FBSDKLoginManager()
        login.logInWithReadPermissions(["public_profile", "email"], fromViewController: self) { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            //ProgressHUD.show("Authenticating...")
            let fbRequest = FBSDKGraphRequest(graphPath:"me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]);
            fbRequest.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
                
                if error == nil {
                    
                    print("User Info : \(result)")
                    let faceBookUserDetails = result as? NSDictionary
                    
                    //User Info : {
                    //    email = "crab1990@gmail.com";
                    //    "first_name" = Aswin;
                    //    gender = male;
                    //    id = 899971410072384;
                    //    "last_name" = Babu;
                    //    name = "Aswin Babu";
                    //    picture =     {
                    //        data =         {
                    //            "is_silhouette" = 0;
                    //            url = "https://scontent.xx.fbcdn.net/hprofile-xaf1/v/t1.0-1/p200x200/10410281_840092816060244_974976197369468421_n.jpg?oh=1e91cf8192669deb739535a905edbabc&oe=5694D227";
                    //        };
                    //    };
                    //}
                   
                    do
                    {
                        User.loginUser(faceBookUserDetails!["email"] as? String, firstName: faceBookUserDetails!["first_name"] as? String, lastName: faceBookUserDetails!["last_name"] as? String, socialType: "f", socialId: faceBookUserDetails!["id"] as? String, profilePicture: ((faceBookUserDetails!["picture"] as! NSDictionary)["data"] as! NSDictionary)["url"] as? String, deviceToken: "1234567890", completion: { (success, result, error) -> Void in
                            
                            if success {
                                print("user lastnaem \(User.currentUser().lastName)")
                                User.currentUser().socialType = "fb"
                                NSUserDefaults.standardUserDefaults().removeObjectForKey("UserDetails")
                                NSUserDefaults.standardUserDefaults().synchronize()
                                let userObject = NSKeyedArchiver.archivedDataWithRootObject(User.currentUser())
                                NSUserDefaults.standardUserDefaults().setObject(userObject, forKey: "UserDetails")
                                NSUserDefaults.standardUserDefaults().synchronize()
                                AppDelegate.sharedInstance().didLogin()
                                //self.delegate!.didLogin()
                            } else {
                                self.imageViewSplash.hidden = true
                            }
                            ProgressHUD.dismiss()
                            
                        })
                    }
//                    catch let _ as NSError {
//                    }
                    
                    
                    
                } else {
                    ProgressHUD.dismiss()
                    print("Error Getting Info \(error)");
                    self.imageViewSplash.hidden = true
                }
            }
        }
    }
    
    

    @IBAction func linkedInLoginButtonAction(sender: AnyObject) {
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.imageViewSplash.hidden = false
        }

        LISDKSessionManager.createSessionWithAuth([LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION], state: "some state", showGoToAppStoreDialog: true, successBlock: { (string) -> () in
           // ProgressHUD.show("Authenticating...")
                if (LISDKSessionManager.hasValidSession()) {
                    LISDKAPIHelper.sharedInstance().getRequest("\(LINKEDIN_API_URL)/people/~:(id,firstName,lastName,email-address,picture-url)", success: { (response) -> () in
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> () in
                            
//                            UIAlertView(title: "Message", message: "LinkedIn login successful" + response.data, delegate: nil, cancelButtonTitle: "OK").show()
                            
                            
                            if let dataFromString = response.data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
                                var json: AnyObject?
                                do {
                                    json = try NSJSONSerialization.JSONObjectWithData(dataFromString, options: NSJSONReadingOptions())
                                } catch _ as NSError {
                                    //            error = error1
                                    json = nil
                                }
                            
                                
                                do
                                {
                                    User.loginUser(json!["emailAddress"] as? String, firstName: json!["firstName"] as? String, lastName: json!["lastName"] as? String, socialType: "l", socialId: json!["id"] as? String, profilePicture: json!["pictureUrl"] as? String, deviceToken: "1234567890", completion: { (success, result, error) -> Void in
                                        
                                        if success {
                                            print("user lastnaem \(User.currentUser().lastName)")
                                            User.currentUser().socialType = "li"
                                            NSUserDefaults.standardUserDefaults().removeObjectForKey("UserDetails")
                                            NSUserDefaults.standardUserDefaults().synchronize()
                                            let userObject = NSKeyedArchiver.archivedDataWithRootObject(User.currentUser())
                                            NSUserDefaults.standardUserDefaults().setObject(userObject, forKey: "UserDetails")
                                            NSUserDefaults.standardUserDefaults().synchronize()
                                            AppDelegate.sharedInstance().didLogin()
                                        } else {
                                            self.imageViewSplash.hidden = true

                                        }
                                        ProgressHUD.dismiss()
//                                        self.delegate!.didLogin()
                                    })
                                }
//                                catch let _ as NSError {
//                                }
//                                LISDKSessionManager.clearSession()
//                                
//                                var socialMember = SocialMember()
//                                socialMember.socialType = SocialType.linkedin
//                                socialMember.id = result["id"].stringValue
//                                socialMember.email = result["emailAddress"].stringValue
//                                socialMember.firstName = result["firstName"].stringValue
//                                socialMember.lastName = result["lastName"].stringValue
//                                
//                                Loader.setOnView(self.view, withTitle: "Loading...", animated: true)
//                                
//                                self.socialLink(socialMember, completion: { () -> () in
//                                    Loader.hideFromView(self.view, animated: true)
//                                })
                            } else {
                                ProgressHUD.dismiss()
                                self.imageViewSplash.hidden = true

                            }
                        })
                        }, error: { (error) -> () in
                            
                            LISDKAPIHelper.sharedInstance().cancelCalls()
                            LISDKSessionManager.clearSession()
                            ProgressHUD.dismiss()
                            self.imageViewSplash.hidden = true

                            print(error)
                    })
                }
                }, errorBlock: { (error) -> () in
                    ProgressHUD.dismiss()
                    LISDKAPIHelper.sharedInstance().cancelCalls()
                    LISDKSessionManager.clearSession()
                    self.imageViewSplash.hidden = true

                    print(error)
            })
        }
    
    @IBAction func buttonActionTerms(sender: AnyObject) {
        let termsVC = self.storyboard?.instantiateViewControllerWithIdentifier("kTermsVC") as! TermsViewController
        let navVC = UINavigationController(rootViewController: termsVC)
        self.presentViewController(navVC, animated: true, completion: nil)
        
    }
    
}
