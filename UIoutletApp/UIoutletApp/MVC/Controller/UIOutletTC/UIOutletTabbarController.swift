//
//  UIOutletTabbarController.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class UIOutletTabbarController: UITabBarController, UITabBarControllerDelegate, UINavigationControllerDelegate {
    
    /*
    TabBarTransitionController * tabBarTransitionController;
    @property (strong, nonatomic) TabBarControllerDelegate * tabBarControllerDelegate;
*/
    var tabBarTransitionController : TabBarTransitionController!
    var tabBarControllerDelegate : TabBarControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let animator = ScrollTransition()
        self.tabBarControllerDelegate = TabBarControllerDelegate(animator: animator)
        self.delegate = self.tabBarControllerDelegate
        self.tabBarTransitionController = TabBarTransitionController(tabBarController: self, tabBarControllerDelegate: self.tabBarControllerDelegate)
        
//        self.delegate = self
        
        //        self.edgesForExtendedLayout = UIRectEdge.None
        self.tabBar.frame = CGRectMake(CGRectGetMinX(self.tabBar.frame), CGRectGetMinY(self.view.bounds), CGRectGetWidth(self.tabBar.frame), CGRectGetHeight(self.tabBar.frame));
        self.tabBar.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin;UIViewAutoresizing.FlexibleLeftMargin;UIViewAutoresizing.FlexibleWidth;UIViewAutoresizing.FlexibleRightMargin;
        
        print("tab frame \(self.tabBar.frame)")
        
        let settingsButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "settings_icon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(UIOutletTabbarController.settingsButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = settingsButton;
        
        
        var titleView : UIImageView
        titleView = UIImageView(frame:CGRectMake(0, 0, 74, 20))
        titleView.contentMode = .ScaleAspectFit
        titleView.image = UIImage(named: "Nav_logo")
        
        self.navigationItem.titleView = titleView
        
        //
        //        for parent in self.navigationController!.navigationBar.subviews {
        //            for childView in parent.subviews {
        //                if(childView is UIImageView) {
        //                    childView.removeFromSuperview()
        //                }
        //            }
        //        }
        
        self.setTabIcon()
        if let _ = (self.viewControllers?.first as? UINavigationController)?.viewControllers.first as? DesignDetailViewController {
            self.navigationController?.navigationBarHidden = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBar.frame = CGRectMake(CGRectGetMinX(self.tabBar.frame), CGRectGetMinY(self.view.bounds), CGRectGetWidth(self.tabBar.frame), CGRectGetHeight(self.tabBar.frame));
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeGesture() {
        self.tabBarTransitionController.removeGestureRecognizer()
    }
    
    func enableGesture() {
        self.tabBarTransitionController.enableGestureRecognizer()
    }
    
    //MARK: - ButtonAction
    func settingsButtonAction(sender : AnyObject) {
        let settingsNC : UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("SettingsNC") as! UINavigationController
        (settingsNC.viewControllers[0] as! SettingsViewController).delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.presentViewController(settingsNC, animated: true, completion: nil)
        
    }
    
    //MARK: - SetTabAppearance
    func setTabIcon() {
        let forYouTab = self.tabBar.items?[0]
//        ForYouSelected
        forYouTab?.selectedImage = UIImage(named: "ForYou")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        forYouTab?.image = UIImage(named: "ForYou")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        forYouTab?.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        
//        FavoritesSelected
        let favoriteTab = self.tabBar.items?[1]
        favoriteTab?.selectedImage = UIImage(named: "Favorites")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        favoriteTab?.image = UIImage(named: "Favorites")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        favoriteTab?.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        
//        SketchFilesSelected
        let sketchTab = self.tabBar.items?[3]
        sketchTab?.selectedImage = UIImage(named: "SketchFiles")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        sketchTab?.image = UIImage(named: "SketchFiles")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        sketchTab?.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        
//        PSDFilesSelected
        let psdTab = self.tabBar.items?[2]
        psdTab?.selectedImage = UIImage(named: "PSDFiles")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        psdTab?.image = UIImage(named: "PSDFiles")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        psdTab?.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
    }
    
    
//    //MARK: - UITabbarControllerDelegate
//    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
//        
//        
//        
//        let controllerIndex = tabBarController.viewControllers?.indexOf(viewController)
//        
//        if (controllerIndex == tabBarController.selectedIndex) {
//            return false
//        }
//        
//        let fromView = tabBarController.selectedViewController?.view
//        let toView = (tabBarController.viewControllers![controllerIndex!]).view
//        
//        let viewSize = fromView?.frame
//        
//        let scrollLeft : Bool = controllerIndex > tabBarController.selectedIndex
//        
//        fromView?.superview?.addSubview(toView)
//        
//        let screenWidth = UIScreen.mainScreen().bounds.size.width
//        toView.frame = CGRectMake(scrollLeft ? screenWidth : -screenWidth, (viewSize?.origin.y)!, screenWidth, (viewSize?.size.height)!)
//        
//        
//        UIView.animateWithDuration(0.25, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
//            
//            fromView?.frame = CGRectMake(scrollLeft ? -screenWidth : screenWidth, (viewSize?.origin.y)!, screenWidth, (viewSize?.size.height)!)
//            toView.frame = CGRectMake(0.0, (viewSize?.origin.y)!, screenWidth, (viewSize?.size.height)!)
//            
//            }) { (finished : Bool) -> Void in
//                
//                if (finished) {
//                    fromView?.removeFromSuperview()
//                    tabBarController.selectedIndex = controllerIndex!
//                }
//        }
//        
//        return false
//
//    }

    /*
    //MARK: UINavigationControllerDelegate
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        
        if (operation == UINavigationControllerOperation.Pop) {
            return CustomPopNavigationAnimation()
            
        }
        else {
            
            //            fromVC.beginAppearanceTransition(true, animated: true)
            //            toVC.beginAppearanceTransition(true, animated: true)
            return CustomNavigationPushAnimation()
        }
    }
       
*/
    
}


