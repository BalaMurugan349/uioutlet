//
//  TermsViewController.swift
//  UIoutletApp
//
//  Created by Bose on 11/08/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Terms & Conditions"
        let barButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(buttonActionClose))
        self.navigationItem.rightBarButtonItem = barButtonItem
        webView.loadRequest(NSURLRequest(URL: NSURL(string: "http://uioutlet.net/terms.html")!))
        // Do any additional setup after loading the view.
    }
    
    func buttonActionClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension TermsViewController : UIWebViewDelegate {
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        activityIndicator.stopAnimating()
    }
}
