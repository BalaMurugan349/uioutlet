//
//  DesignDetailViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 03/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit
import MessageUI

protocol DesignDetailViewControllerDelegate {
    func didChangeFavoriteStatusOfDesign(design :UIDesigns?)
}

class DesignDetailViewController: UITableViewController, MFMailComposeViewControllerDelegate, UIAlertViewDelegate, UIViewControllerTransitioningDelegate, UITextViewDelegate {
    
    @IBOutlet weak var textViewDesignDescription: UITextView!
    @IBOutlet weak var imageViewMainDesign: UIImageView!
    @IBOutlet weak var labelDesignName: UILabel!
    @IBOutlet weak var labelDesignDate: UILabel!
    @IBOutlet weak var buttonAuthorName: UIButton!
    @IBOutlet weak var buttonFavorite: UIButton!
    @IBOutlet weak var buttonDownload: UIButton!
    @IBOutlet weak var imageViewDocumentType1: UIImageView!
    @IBOutlet weak var imageViewDocumentType2: UIImageView!
    @IBOutlet weak var constraintLabelDesignNameHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintLabelTopMargin: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewFooter: UIView!
    
    
    var galleryViewController: GalleryViewController?
    var galleryNavigationController : UINavigationController?
    var delegate: DesignDetailViewControllerDelegate?
    var designDetails : UIDesigns?
    var newTextViewHeight : CGFloat!
    var designNewID : String?
    var headerView : ParallaxHeaderView!
    var isPushed : Bool!
    
    var tapGestureGradientBar : UITapGestureRecognizer!
    
    let customPresentAnimationController = CustomPresentAnimation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isPushed = false
        newTextViewHeight = 0.0
        self.navigationController?.navigationBarHidden = true
        self.tabBarController?.tabBar.hidden = true
        resetBadge()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tapGestureGradientBar = UITapGestureRecognizer(target: self, action: #selector(DesignDetailViewController.showGalleryView(_:)))
        
        viewHeader.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 168)
        viewFooter.frame = CGRectMake(0, self.view.frame.size.height - 75, self.tableView.frame.size.width, 75)
        self.view.addSubview(viewFooter)
        if self.designDetails != nil {
            self.setUIDesign()
        }
        else if self.designNewID != nil {
//            print("shared design \(self.designNewID)")
            UIDesigns.getDesignDetail(self.designNewID, completion: { (success, result, error) -> Void in
                if success {
                    self.designDetails = result as? UIDesigns
                    self.setUIDesign()
                    if (self.designDetails != nil && (self.designDetails?.designAuthorWebURL?.characters.count) == nil  && (self.designDetails!.designAuthorWebURL!.isEmpty)) {
                        //                        self.buttonAuthorName.setImage(nil, forState: UIControlState.Normal)
                    }
                    if (self.designDetails == nil || (self.designDetails?.designAuthorWebURL == nil || (self.designDetails?.designAuthorWebURL?.characters.count) == nil  && (self.designDetails!.designAuthorWebURL!.isEmpty))) {
                        self.buttonAuthorName.setImage(nil, forState: UIControlState.Normal)
                    }
                }
            })
        }
        
    }
    
    func resetBadge()  {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        let manager  = AFHTTPRequestOperationManager()
        let params : NSDictionary = ["user" : User.currentUser().id!]
        
        manager.GET("http://uioutlet.net/JasonUX/jasonux/index.php/api/user/reset_badge", parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
//            let result : NSDictionary = responseObject as! NSDictionary
//            print("result = \(result)")
        }) { (operation, error) -> Void in
//            print("Error = \(error.localizedDescription)")
        }
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        UIApplication.sharedApplication().statusBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        let outletTabbarController = self.navigationController?.tabBarController as? UIOutletTabbarController
        outletTabbarController?.removeGesture()
        
        if (self.designDetails == nil || (self.designDetails?.designAuthorWebURL == nil || (self.designDetails?.designAuthorWebURL?.characters.count) == nil  && (self.designDetails!.designAuthorWebURL!.isEmpty))) {
            buttonAuthorName.setImage(nil, forState: UIControlState.Normal)
        }
        self.navigationController?.navigationBarHidden = true
        self.checkDescriptionSize()

        
        //        self.view.addSubview(viewHeader)
        //        self.navigationController?.view.addSubview(viewHeader)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        isPushed = false
    }
    
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false;
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //        viewHeader.removeFromSuperview()
        isPushed = true
        UIApplication.sharedApplication().statusBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        self.delegate?.didChangeFavoriteStatusOfDesign(self.designDetails)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUIDesign() {
        
        let arrayGalleryList : NSMutableArray = NSMutableArray()
        
        self.constraintLabelTopMargin.constant = 10.0
        
        if self.designDetails?.designGalleryList?.count > 0 {
            arrayGalleryList.addObjectsFromArray(self.designDetails?.designGalleryList as! [AnyObject])
        }
        
        if self.designDetails?.designGalleryVideoList?.count > 0 {
            arrayGalleryList.addObjectsFromArray(self.designDetails?.designGalleryVideoList as! [AnyObject])
        }
        
        if arrayGalleryList.count > 0 {
            galleryViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GalleryVC") as? GalleryViewController
            if (self.designDetails != nil && (self.designDetails?.designName?.characters.count) != nil  && !(self.designDetails!.designName!.isEmpty)) {
                galleryViewController?.designName = self.designDetails?.designName!
            }
            galleryViewController?.imageItemsCount = (self.designDetails?.designGalleryList?.count)!
            galleryViewController!.setDesignGalleryList(arrayGalleryList as NSArray)
            galleryNavigationController = UINavigationController(rootViewController: galleryViewController!)
            galleryNavigationController?.transitioningDelegate = self
        }

        
        
        self.imageViewMainDesign.sd_setImageWithURL(NSURL(string: ImageDomainURL + (self.designDetails?.designThumbnail)!)) { (image, error, cacheType, url) in
            self.imageViewMainDesign.image = image
            if let _ = self.headerView {
                self.headerView.headerImage = self.imageViewMainDesign.image
            }
        }
        
        
//        let imgvw : UIImageView = UIImageView(frame: CGRectMake(0, 0, 320, 72))
//        imgvw.image = self.imageViewMainDesign.image
//        imgvw.contentMode = .ScaleAspectFill
//        (UIApplication.sharedApplication().delegate as! AppDelegate).window?.addSubview(imgvw)
        
        //        self.headerView.imageView!.sd_setImageWithURL(NSURL(string: ImageDomainURL + (self.designDetails?.designThumbnail)!))
        if ((self.designDetails?.designSketchFile?.characters.count) != nil  && !(self.designDetails!.designSketchFile!.isEmpty)) {
            self.imageViewDocumentType1.image = UIImage(named: "Sketch_doc_icon")
            if ((self.designDetails?.designPsdFile?.characters.count) != nil  && !(self.designDetails!.designPsdFile!.isEmpty)) {
                self.imageViewDocumentType2.image = UIImage(named: "Photoshop_doc_icon")
            }
        }
        else {
            if ((self.designDetails?.designPsdFile?.characters.count) != nil  && !(self.designDetails!.designPsdFile!.isEmpty)) {
                self.imageViewDocumentType1.image = UIImage(named: "Photoshop_doc_icon")
            }
        }
        
        
        var designDate : String?
        
        if ((self.designDetails?.designUpdatedDate?.characters.count) != nil && !(self.designDetails!.designUpdatedDate!.isEmpty)) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateUpdated : NSDate? = dateFormatter.dateFromString((self.designDetails?.designUpdatedDate)!)
            if dateUpdated != nil {
                if ((self.designDetails?.designCreatedDate?.characters.count) != nil && !(self.designDetails!.designCreatedDate!.isEmpty)) {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let dateCreated : NSDate? = dateFormatter.dateFromString((self.designDetails?.designCreatedDate)!)
                    dateFormatter.dateFormat = "M/d/yy"
                    if dateCreated != nil{
                        designDate = dateFormatter.stringFromDate(dateCreated!)
                        labelDesignDate.text = "Uploaded by " + designDate!
                    }
                }
                else {
                    dateFormatter.dateFormat = "M/d/yy"
                    if dateUpdated != nil{
                        designDate = dateFormatter.stringFromDate(dateUpdated!)
                        labelDesignDate.text = "Updated by " + designDate!
                        
                    }
                }
                
            }
        }
        else if ((self.designDetails?.designCreatedDate?.characters.count) != nil && !(self.designDetails!.designCreatedDate!.isEmpty)) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateCreated : NSDate? = dateFormatter.dateFromString((self.designDetails?.designCreatedDate)!)
            dateFormatter.dateFormat = "M/d/yy"
            if dateCreated != nil{
                designDate = dateFormatter.stringFromDate(dateCreated!)
                labelDesignDate.text = "Uploaded by " + designDate!
            }
        }
        
        labelDesignName.text = self.designDetails?.designName
        self.fixTitleHeight()
        if ((self.designDetails?.designAuthorName?.characters.count) != nil && !(self.designDetails!.designAuthorName!.isEmpty)) {
            buttonAuthorName.setTitle("by " + (self.designDetails?.designAuthorName)! + "  ", forState: UIControlState.Normal)
        }
        buttonAuthorName.sizeToFit()
        //        UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, left: CGFloat, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
        buttonAuthorName.titleEdgeInsets = UIEdgeInsetsMake(0, -((buttonAuthorName.imageView?.frame.size.width)!), 0, (buttonAuthorName.imageView?.frame.size.width)!)
        buttonAuthorName.imageEdgeInsets = UIEdgeInsetsMake(0, (buttonAuthorName.titleLabel?.frame.size.width)!, 0, -((buttonAuthorName.titleLabel?.frame.size.width)!))
        //        buttonAuthorName.titleEdgeInsets = UIEdgeInsetsMake(0, -buttonAuthorName.imageView.frame.size.width, 0, buttonAuthorName.imageView.frame.size.width)
        //        buttonAuthorName.imageEdgeInsets = UIEdgeInsetsMake(0, buttonAuthorName.titleLabel.frame.size.width, 0, -buttonAuthorName.titleLabel.frame.size.width);
        //        buttonFavorite.selected = (self.designDetails!.designIsFavorite).
        
        if self.designDetails?.designIsFavorite == nil{
            self.buttonFavorite.enabled = true
        }
        else
        {
            if self.designDetails?.designIsFavorite == "1" {
                self.buttonFavorite.selected = true
            }
            else {
                self.buttonFavorite.selected = false
            }
        }
        
        //        self.textViewDesignDescription.scrollEnabled = true
        
        //        NSString *htmlString = @"<h1>Header</h1><h2>Subheader</h2><p>Some <em>text</em></p><img src='http://blogs.babble.com/famecrawler/files/2010/11/mickey_mouse-1097.jpg' width=70 height=100 />";
        //        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        //        textView.attributedText = attributedString;
        
        
        
        
        if ((self.designDetails?.designDescription?.characters.count) != nil && !(self.designDetails!.designDescription!.isEmpty)) {
            var descriptionText = self.designDetails?.designDescription
//            descriptionText = descriptionText?.stringByReplacingOccurrencesOfString("\\u2022", withString: "•")
//            descriptionText = descriptionText?.stringByReplacingOccurrencesOfString("\n", withString: "<br/>")
//            descriptionText = descriptionText?.stringByReplacingOccurrencesOfString("\r", withString: "")
            do {
                self.textViewDesignDescription.editable = false
//                self.textViewDesignDescription.selectable = false
                self.textViewDesignDescription.scrollEnabled = false
//                descriptionText = [des stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>", self.font.fontName, self.font.pointSize]];
                
                descriptionText = descriptionText?.stringByAppendingString(NSString(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>",".HelveticaNeueDeskInterface-Regular"/* ".HelveticaNeueDeskInterface"*/, 16.0) as String)
                
                
                let attributedDescription = try NSMutableAttributedString(data: (descriptionText?.dataUsingEncoding(NSUnicodeStringEncoding))!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
        
//               attributedDescription.addAttribute(NSFontAttributeName, value:  UIFont(name: ".HelveticaNeueDeskInterface-Regular", size: 15)!, range: NSMakeRange(0, attributedDescription.length))
                
                
//                let paragraphStyle = NSMutableParagraphStyle()
//                paragraphStyle.alignment = NSTextAlignment.Justified
//                
//                attributedDescription.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedDescription.length))
                
                
                self.textViewDesignDescription.attributedText = attributedDescription
                
            } catch {
                print(error)
            }
            
        }
        
        //        self.textViewDesignDescription.text = self.designDetails?.designDescription
        //        self.textViewDesignDescription.sizeToFit()
        //        self.textViewDesignDescription.layoutIfNeeded()
        //        self.textViewDesignDescription.layoutManager.allowsNonContiguousLayout = false
        
        //        print("text viewheight : \(self.textViewDesignDescription.frame.size.height)")
        //        let fixedWidth = self.textViewDesignDescription.frame.size.width
        //        let newSize = self.textViewDesignDescription.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        //        var newFrame = self.textViewDesignDescription.frame
        //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        //        self.textViewDesignDescription.frame = newFrame;
        //        print("modifiedtext viewheight : \(newSize.height)")
        //        self.textViewDesignDescription.scrollEnabled = false
        //        newTextViewHeight = newSize.height
        
        //        ParallaxHeaderView *headerView = [ParallaxHeaderView parallaxHeaderViewWithCGSize:CGSizeMake(self.mainTableView.frame.size.width, 300)];
        //        headerView.headerTitleLabel.text = self.story[@"story"];
        //        headerView.headerImage = [UIImage imageNamed:@"HeaderImage"];
        
        headerView = ParallaxHeaderView.parallaxHeaderViewWithCGSize(CGSizeMake(self.tableView.frame.size.width, 350)) as! ParallaxHeaderView
        //        headerView.headerTitleLabel.text = "helo"
        headerView.headerImage = self.imageViewMainDesign.image
        
//        let buttonPreview : UIButton = UIButton(type: .Custom)
//        buttonPreview.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 50, headerView.frame.size.height - 80, 100, 35)
//       // buttonPreview.setTitle("Preview Design", forState: UIControlState.Normal)
//        buttonPreview.setImage(UIImage(named: "Preview"), forState: UIControlState.Normal)
//        buttonPreview.addTarget(self, action: #selector(DesignDetailViewController.showGalleryView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//        buttonPreview.titleLabel?.font = UIFont.systemFontOfSize(15.0)
//        headerView.addSubview(buttonPreview)
//        headerView.bringSubviewToFront(buttonPreview)
        //                headerView.imageScrollView!.addSubview(viewHeader)
        //        headerView.addSubview(viewHeader)
        self.view.addSubview(viewHeader)
        //        viewHeader.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 168)
//        let tapGestureHeaderImage = UITapGestureRecognizer(target: self, action: #selector(DesignDetailViewController.showGalleryView(_:)))
//        headerView.imageView!.addGestureRecognizer(tapGestureHeaderImage)
        
//        viewHeader!.addGestureRecognizer(tapGestureGradientBar)
        
        self.tableView.tableHeaderView = headerView
        
    }
    
    
    
    func checkDescriptionSize() {
        
        self.view.layoutIfNeeded()
//        print("text viewheight : \(self.textViewDesignDescription.frame.size.height)")
        let fixedWidth = self.textViewDesignDescription.frame.size.width
        let newSize = self.textViewDesignDescription.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
//        var newFrame = self.textViewDesignDescription.frame
//        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
//        self.textViewDesignDescription.frame = newSize;
//        print("modifiedtext viewheight : \(newSize.height)")
//        self.textViewDesignDescription.scrollEnabled = false
        newTextViewHeight = newSize.height
        self.tableView.reloadData()
    }
    
    //MARK: - FixTitleHeight
    func fixTitleHeight() {
        self.view.layoutIfNeeded()
        let attributes = [NSFontAttributeName : labelDesignName.font]
        labelDesignName.numberOfLines = 0
        labelDesignName.lineBreakMode = NSLineBreakMode.ByWordWrapping
        let rect = labelDesignName.text!.boundingRectWithSize(CGSizeMake(labelDesignName.frame.size.width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil)
        constraintLabelDesignNameHeight.constant = rect.size.height
    }
    
    
    // MARK: TableView
    // MARK:  TableView DataSource
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //        if indexPath.row == 0 {
        //            return 350.0
        //        }
        //        else {
        var currentHeight : CGFloat = 300.0
//        currentHeight = currentHeight + 170.0
        
        
//        if newTextViewHeight > 110.0 {
//            let value : CGFloat = newTextViewHeight - 110.0
//            currentHeight = currentHeight + value;
//        }
        
//        return currentHeight + 85.0
        
        let calculatedHeight : CGFloat = constraintLabelDesignNameHeight.constant + newTextViewHeight + 21 + 32 + 51 + 28 + 90
        if (currentHeight < calculatedHeight) {
            currentHeight = calculatedHeight
        }
        
            return currentHeight
        
//        return constraintLabelDesignNameHeight.constant + newTextViewHeight + 18 + 40 + 28+21 + 14 + 11+18
        
//        return 18+40+28
        //        }
    }
    //
    //    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        return 75.0
    //    }
    //
    //    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        return viewFooter
    //    }
    
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        self.headerView.layoutHeaderViewForScrollViewOffset(scrollView.contentOffset)
        
        //        let offsetMargin = ((-1) * scrollView.contentOffset.y)
        
        //        if (offsetMargin >= 150.0 && !isPushed) {
        //            isPushed = true
        //            self.showGalleryView(viewHeader)
        //        }
//        print("offset = \(scrollView.contentOffset)")
        //        if (scrollView.contentOffset.y > 210.0) {
        //
        //
        //            self.viewHeader.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 64)
        //        }
        //        else {
        //            //                self.constraintLabelTopMargin.constant = 10.0
        //            self.viewHeader.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 168)
        //        }
        
        
        //        if (scrollView.contentOffset.y <= 0) {
        //            self.viewHeader.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 168)
        //        }
        //        else {
        
        var height : CGFloat = 168
        
        if (scrollView.contentOffset.y + 168 < 350) {
            imageViewMainDesign.hidden = true
            height = 168
        }
        else {
            var variableHeight = scrollView.contentOffset.y
            if (variableHeight > 168) {
                variableHeight = variableHeight - 183
            }
            height = 168 - variableHeight
            if (height <= 72) {
                imageViewMainDesign.hidden = false
                height = 72
            }
            else {
                imageViewMainDesign.hidden = true
            }
        }
        
//        print("height = \(height)")
//        print("width = \(self.tableView.frame.size.width)")
        self.viewHeader.frame = CGRectMake(0, scrollView.contentOffset.y, self.tableView.frame.size.width, height)
        //        }
        
        
        //        constraintLabelTopMargin.constant = constraintLabelTopMargin.constant + scrollView.contentOffset.y
        
        
        let footerFrame = viewFooter.frame
        let newOriginY = self.tableView.contentOffset.y + self.tableView.frame.size.height - footerFrame.size.height
        let newFooterFrame = CGRectMake(footerFrame.origin.x, newOriginY, footerFrame.size.width, footerFrame.size.height)
        viewFooter.frame = newFooterFrame
    }
    
    
    //MARK: - Gesture
    @IBAction func showGalleryView(sender: AnyObject) {
        print("gallery View")
        
        if (viewHeader.frame.size.height < 80) {
            return
        }
        
        
        if self.designDetails?.designGalleryList?.count > 0 || self.designDetails?.designGalleryVideoList?.count > 0 {
            self.presentViewController(galleryNavigationController!, animated: true, completion: { () -> Void in
            })
        }
        
    }
    
    //MARK: - ButtonAction
    
    @IBAction func backButtonAction(sender: AnyObject) {
        if let _ = (self.tabBarController?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? DesignDetailViewController {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.setMainView()
        } else {
            if self.navigationController?.viewControllers.count == 1 {
                self.navigationController?.dismissViewControllerAnimated(true, completion:nil)
            } else {
                // self.navigationController?.popViewControllerAnimated(false)
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
        }        
    }
    
    @IBAction func authorNameButtonAction(sender: AnyObject) {
        if ((self.designDetails?.designAuthorWebURL?.characters.count) != nil  && !(self.designDetails!.designAuthorWebURL!.isEmpty)) {
            UIApplication.sharedApplication().openURL(NSURL(string: (self.designDetails?.designAuthorWebURL)!)!)
        }
        
        
        
    }
    
    @IBAction func shareButtonAction(sender: AnyObject) {
        
        //        if self.imageViewMainDesign.image != nil {
        //        let shareImage: UIImage = self.imageViewMainDesign.image!
        //        }
        //
        if let webURL = NSURL(string: "http://uioutlet.net/JasonUX/jasonux/index.php/document/share?id=" + (self.designDetails?.designId)!)
            //        if let webURL = NSURL(string: "")
        {
            var objectsToShare : [AnyObject]
            //            if let shareImage = self.imageViewMainDesign.image {
            //                objectsToShare = [webURL, shareImage]
            //            }
            
            //BALA
           let shareText : String = "UIOutlet"//"I Just found this cool UI theme: " + (self.designDetails?.designName)! + " on #UIO"
            
            
            if let shareImage = self.headerView.imageView!.image {
                
                objectsToShare = [shareText, webURL, shareImage]
                
            }
            else {
                    objectsToShare = [shareText,webURL]
            }
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
            
            activityVC.excludedActivityTypes =  [
                UIActivityTypePostToTwitter,
                UIActivityTypePrint,
                UIActivityTypeCopyToPasteboard,
                UIActivityTypeAssignToContact,
//                UIActivityTypeSaveToCameraRoll,
//                UIActivityTypeAddToReadingList,
//                UIActivityTypePostToFlickr,
//                UIActivityTypePostToVimeo,
//                UIActivityTypePostToTencentWeibo
            ]
            
//            UIActivityTypePostToFacebook,
//            UIActivityTypePostToTwitter,
//            UIActivityTypePostToWeibo,
//            UIActivityTypeMessage,
//            UIActivityTypeMail,
//            UIActivityTypePrint,
//            UIActivityTypeCopyToPasteboard,
//            UIActivityTypeAssignToContact,
//            UIActivityTypeSaveToCameraRoll,
//            UIActivityTypeAddToReadingList,
//            UIActivityTypePostToFlickr,
//            UIActivityTypePostToVimeo,
//            UIActivityTypePostToTencentWeibo,

            
            
            /*

            UIActivityTypePostToFacebook
            UIActivityTypePostToTwitter
            UIActivityTypePostToWeibo
            UIActivityTypeMessage
            UIActivityTypeMail
            UIActivityTypePrint
            UIActivityTypeCopyToPasteboard
            UIActivityTypeAssignToContact
            UIActivityTypeSaveToCameraRoll
            UIActivityTypeAddToReadingList
            UIActivityTypePostToFlickr
            UIActivityTypePostToVimeo
            UIActivityTypePostToTencentWeibo
            UIActivityTypeAirDrop
            UIActivityTypeOpenInIBooks


*/
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func favoriteButtonAction(sender: UIButton) {
        self.buttonFavorite.selected = !self.buttonFavorite.selected

        if self.buttonFavorite.selected {
        UIView.animateWithDuration(0.3/1.5, animations: {
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        }) { (finished) in
            UIView.animateWithDuration(0.3/2, animations: {
                sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                }, completion: { (finished) in
                    UIView.animateWithDuration(0.3/2, animations: {
                        sender.transform = CGAffineTransformIdentity;
                    })
            })
        }
        }

       var serviceRequest : String
        if !self.buttonFavorite.selected {
            serviceRequest = "remove"
        }
        else {
            serviceRequest = "add"
        }
        
        self.designDetails?.addDesignFavoriteForUser(User.currentUser().id, service: serviceRequest, completion: { (success, result, error) -> Void in
            if success{
                if self.buttonFavorite.selected {
                    self.designDetails?.designIsFavorite = "1"
                }
                else {
                    self.designDetails?.designIsFavorite = "0"
                }
                
                
                 NSNotificationCenter.defaultCenter().postNotificationName("DesignFavoriteUpdateNotification", object: nil, userInfo: ["designChanged" : self.designDetails!])
                
                //                self.delegate?.didChangeFavoriteStatusOfDesign(self.designDetails)
            }
            
        })
    }
    
    @IBAction func downloadButtonInitialAction(sender: AnyObject) {
        let senderButton : UIButton = sender as! UIButton
        //        senderButton.setTitleColor(UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.9), forState: UIControlState.Normal)
        senderButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        senderButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Selected)
        senderButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Highlighted)
        senderButton.backgroundColor = UIColor.init(red: 2.0/255.0, green: 119.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    @IBAction func downloadButtonCancelAction(sender: AnyObject) {
        self.changeDownloadButtonToDefault()
    }
    
    @IBAction func downloadButtonAction(sender: AnyObject) {
        
        
        UIAlertView(title: "Download", message: "Would you like to download this file to " + User.currentUser().email! , delegate: self, cancelButtonTitle: "Yes", otherButtonTitles: "No").show()
    }
    
    //    func changeDownloadButtonToDefault(timer:NSTimer) {
    //        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
    //        let downloadButton : UIButton = (userInfo["downloadButton"] as! UIButton)
    //
    //        downloadButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    //        downloadButton.backgroundColor = UIColor.init(red: 2.0/255.0, green: 139.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    //    }
    
    func changeDownloadButtonToDefault() {
        buttonDownload.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        buttonDownload.backgroundColor = UIColor.init(red: 2.0/255.0, green: 139.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    //MARK: - UIAlertViewDelegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        self.changeDownloadButtonToDefault()
        switch (buttonIndex){
        case alertView.cancelButtonIndex :
//            self.designDetails?.downloadDesignForUser(User.currentUser().id, completion: { (success, result, error) -> Void in
//                if success {
//                    UIAlertView(title: nil, message: "Theme is sent to your email", delegate: nil, cancelButtonTitle: "Thanks").show()
//                }
//            })
            let message : SMTPMessage = SMTPMessage()
            message.from = "uioutletapp@gmail.com"
            message.to = User.currentUser().email!
            message.host = "smtp.gmail.com"
            message.account = "uioutletapp@gmail.com"
            message.pwd = "uioutletsupport123"
            
            var htmlString : String = ""
            if designDetails!.designPsdFile! == "" {
                htmlString = "<p>Hi,</p><div><h4>Thank you for downloading the \(designDetails!.designName!).</h4></div>Here you can download the <strong><a href=\(designDetails!.designSketchFile!) target=_blank>.sketch file</a></strong></div><p><br />Thank you, <br /><br />Support Team</p><p>UIOutlet App</p>"
            }else if designDetails!.designSketchFile! == "" {
                 htmlString = "<p>Hi,</p><div><h4>Thank you for downloading the \(designDetails!.designName!).</h4></div><div>Here you can download the <strong><a href=\(designDetails!.designPsdFile!) target=_blank>PSD file</a></strong></div><p><br />Thank you, <br /><br />Support Team</p><p>UIOutlet App</p>"
            }else{
                 htmlString = "<p>Hi,</p><div><h4>Thank you for downloading the \(designDetails!.designName!).</h4></div><div>Here you can download the <strong><a href=\(designDetails!.designPsdFile!) target=_blank>PSD file</a></strong></div><div>Here you can download the <strong><a href=\(designDetails!.designSketchFile!) target=_blank>.sketch file</a></strong></div><p><br />Thank you, <br /><br />Support Team</p><p>UIOutlet App</p>"
            }
            message.subject = "UIOutlet Design"
            message.content = htmlString
            UIAlertView(title: nil, message: "Theme is sent to your email", delegate: nil, cancelButtonTitle: "Thanks").show()

            message.send({ (messg, now, total) in
                
                }, success: { (messg) in
//                    print("Email sent")
                }, failure: { (messg, error) in
            })
            break
        default :
            break
        }
    }
    
    //MARK: - UIViewControllerTransitionDelegate
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentAnimationController
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentAnimationController
    }
}
