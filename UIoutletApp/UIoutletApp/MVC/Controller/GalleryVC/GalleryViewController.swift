//
//  GalleryViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 17/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit
import MediaPlayer

class GalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, GalleryCollectionViewCellDelegate
{
    
    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var pageControlGallery: UIPageControl!
    var moviePlayer:MPMoviePlayerViewController!
    var pageIndexBarButton : UIBarButtonItem!
    var closeButton : UIBarButtonItem!
    var designName : String!
    var arrayDesignImageList : NSArray!
    var imageItemsCount = 0
    var hideTopBar : Bool!
    var currentPage : Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideTopBar = true
        self.currentPage = 1
        
        self.addBlurEffect()

        
        
        closeButton = UIBarButtonItem.init(title: "Close", style: UIBarButtonItemStyle.Done, target: self, action: #selector(GalleryViewController.backButtonAction(_:)))
        self.pageIndexBarButton = UIBarButtonItem.init(title: "1 out of 10", style: UIBarButtonItemStyle.Done, target: self, action: nil)
        
               self.navigationItem.rightBarButtonItem = closeButton;
        self.navigationItem.leftBarButtonItem = self.pageIndexBarButton
        
        self.setUI()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.pageControlGallery.numberOfPages = arrayDesignImageList.count
        self.pageControlGallery.currentPage = 0
       
    }
    
    func addBlurEffect() {
        // Add blur view
        var bounds = self.navigationController?.navigationBar.bounds as CGRect!
        bounds.offsetInPlace(dx: 0.0, dy: -20.0)
        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark)) as UIVisualEffectView
        visualEffectView.frame = bounds
        visualEffectView.autoresizingMask = [.FlexibleHeight ,.FlexibleWidth]
        self.navigationController?.navigationBar.addSubview(visualEffectView)
        
        // Here you can add visual effects to any UIView control.
        // Replace custom view with navigation bar in above code to add effects to custom view.
    }
    
    //MARK: SetUI
    func setUI() {
        UIApplication.sharedApplication().statusBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.BlackTranslucent
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "gradient_image"), forBarMetrics: UIBarMetrics.Default)
        
        closeButton.setTitleTextAttributes([NSFontAttributeName: UIFont(name: ".HelveticaNeueDeskInterface-Regular", size: 15)!], forState: UIControlState.Normal)
        self.pageIndexBarButton.setTitleTextAttributes([NSFontAttributeName: UIFont(name: ".HelveticaNeueDeskInterface-MediumP4", size: 11)!], forState: UIControlState.Normal)
        
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.setUI()
        
         self.collectionViewImage.reloadData()
        
        if (self.designName != nil && (self.designName?.characters.count) != nil  && !(self.designName!.isEmpty)) {
            self.title = self.designName!
//            self.navigationController?.navigationItem.title = self.designName!
            self.navigationController!.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.whiteColor(),
                    NSFontAttributeName: UIFont(name: ".HelveticaNeueDeskInterface-Bold", size: 18)!]
        }
        
        
        self.pageIndexBarButton.title = String(format: "%d out of %d",self.currentPage,self.arrayDesignImageList.count)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        UIApplication.sharedApplication().statusBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBarHidden = true
    }

    override func prefersStatusBarHidden() -> Bool {
//        return (self.navigationController?.navigationBarHidden)!
        return true
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDesignGalleryList(list : NSArray) {
        arrayDesignImageList = list
    }

    
    // MARK: CollectionView
    // MARK: CollectionView DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayDesignImageList.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            return CGSizeMake(self.view.frame.size.width, collectionView.frame.size.height)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("galleryCellId", forIndexPath: indexPath) as! GalleryCollectionViewCell
        
        cell.setGalleryImage(arrayDesignImageList[indexPath.row] as! String)
        if indexPath.row < self.imageItemsCount {
            cell.playButton.hidden = true
        }
        else {
            cell.playButton.hidden = false
        }
        
        
        cell.delegate = self
        return cell
    }

    // MARK: CollectionView Delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        UIApplication.sharedApplication().statusBarHidden = false
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.setNavigationBarHidden(!(self.navigationController?.navigationBarHidden)!, animated: true)
        
    }
    
    
    // MARK: UIScrollView Delegate
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        UIApplication.sharedApplication().statusBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth:CGFloat = CGRectGetWidth(self.collectionViewImage.frame)
        self.pageControlGallery.currentPage = Int(self.collectionViewImage.contentOffset.x / pageWidth)
        self.pageIndexBarButton.title = String(format: "%d out of %d",Int(self.collectionViewImage.contentOffset.x / pageWidth) + 1 ,self.arrayDesignImageList.count)
        self.currentPage = Int(self.collectionViewImage.contentOffset.x / pageWidth) + 1
    }
    
    // MARK: GalleryCollectionViewCellDelegate
    func didTapPlayButton(videoName: String) {
        let url:NSURL = NSURL(string: VideoDomainURL + videoName)!
        
        moviePlayer = MPMoviePlayerViewController(contentURL: url)
        moviePlayer.moviePlayer.controlStyle = MPMovieControlStyle.Fullscreen
        moviePlayer.moviePlayer.shouldAutoplay = true
        self.presentMoviePlayerViewControllerAnimated(moviePlayer)
    }

//    func playerPlaybackDidFinish() {
//        moviePlayer.dismissMoviePlayerViewControllerAnimated()
//    }

    
     // MARK: ButtonAction
    @IBAction func backButtonAction(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
        self .dismissViewControllerAnimated(true, completion: nil)
    }
    
}
