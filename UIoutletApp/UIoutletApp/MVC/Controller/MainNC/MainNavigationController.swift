//
//  MainNavigationController.swift
//  UIoutletApp
//
//  Created by Aswin on 03/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        self.edgesForExtendedLayout = UIRectEdge.Bottom
        
//        print("frame \(NSStringFromCGRect(self.view.frame))")
//        print("tab frame \(self.tabBarController?.view.frame)")
//        
//        self.tabBarController?.view.frame = CGRectMake(CGRectGetMinX((self.tabBarController?.view.frame)!), CGRectGetMinY((self.tabBarController?.view.frame)!), CGRectGetWidth((self.tabBarController?.view.frame)!), CGRectGetHeight(self.view.frame));

        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate = self
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        
    }
    
//    override func preferredStatusBarStyle() -> UIStatusBarStyle {
//        return UIStatusBarStyle.LightContent
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UINavigationControllerDelegate
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
//        if (operation == UINavigationControllerOperation.Pop) {
//            return CustomPopNavigationAnimation()
//        }
//        else {
//            return CustomNavigationPushAnimation()
//        }
        
        if operation == UINavigationControllerOperation.Push {
            return CustomPushNavigationAnimation()
        }
        else {
            
        }
        
        return nil
    }

}
