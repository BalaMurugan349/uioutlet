//
//  FavoriteListViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 05/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class FavoriteListViewController: UITableViewController, DesignDetailViewControllerDelegate {
    
    @IBOutlet weak var viewNoFavourite: UIView!
    var refreshControlDesignList:UIRefreshControl!
    var spinner: UIActivityIndicatorView!
    var arrayDesigns : NSMutableArray!
    var selectedIndex : NSIndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        
        self.spinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0) // Spinner Colour
        self.spinner.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44)
        self.spinner.hidden = true
       // self.tableView.tableFooterView = spinner
        
        self.refreshControlDesignList = UIRefreshControl()
        //        self.refreshControlDesignList.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControlDesignList.addTarget(self, action: #selector(FavoriteListViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        //        self.tableView.tableHeaderView = self.refreshControlDesignList
        
        //        self.tableView.addSubview(refreshControlDesignList)
        
        //        self.edgesForExtendedLayout = UIRectEdge.Bottom
        arrayDesigns = NSMutableArray()
        ProgressHUD.show("Loading...")
       
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.navigationBarHidden = false
        self.navigationController?.tabBarController?.tabBar.hidden = false;
        self.navigationController?.tabBarController?.navigationController?.navigationBarHidden = false
        
        let outletTabbarController = self.navigationController?.tabBarController as? UIOutletTabbarController
        outletTabbarController?.enableGesture()
        
//        arrayDesigns.removeAllObjects()
        self.LoadInitialDesign()
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.tabBarController?.navigationController?.navigationBarHidden = true
    }
    

    
    override func prefersStatusBarHidden() -> Bool {
        return false;
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func LoadInitialDesign() {
        
        UIDesigns.getDesignListForUser(User.currentUser().id, type: "favourite") { (success, result, error) -> Void in
            if success{
                self.arrayDesigns.removeAllObjects()
                if result != nil && result!.isKindOfClass(NSArray) {
                self.arrayDesigns.addObjectsFromArray(result as! NSArray as [AnyObject])
                }
            }
            ProgressHUD.dismiss()
            
            if self.arrayDesigns.count == 0 {
//                self.tableView.hidden = true
                self.refreshControl = nil
                self.viewNoFavourite.center = CGPointMake(self.view.center.x, self.view.center.y - 60)
                self.view .addSubview(self.viewNoFavourite)
                self.loadMoreDesign()
            }
            else {
//                self.tableView.hidden = false
                self.refreshControl = self.refreshControlDesignList
                self.viewNoFavourite.removeFromSuperview()
            }
            
            self.refreshControlDesignList.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    func refresh(sender:AnyObject)
    {
        if arrayDesigns.count > 0 {
            UIDesigns.getLatestDesignListForUser(User.currentUser().id, type: "favourite", latestDesignId: (arrayDesigns.firstObject as! UIDesigns).designId, completion: { (success, result, error) -> Void in
                if success{
                    let array : NSArray = result as! NSArray as [AnyObject]
                    self.arrayDesigns.removeAllObjects()
                    self.arrayDesigns.addObjectsFromArray(array as [AnyObject])//                    self.arrayDesigns.insertObjects(array as [AnyObject], atIndexes: NSIndexSet.init(indexesInRange: NSMakeRange(0, array.count)))
                    self.loadMoreDesign()

                }
                self.refreshControlDesignList.endRefreshing()
                self.tableView.reloadData()
            })
        }
        else {
            self.LoadInitialDesign()
        }
    }
    
    func loadMoreDesign() {
        
        if arrayDesigns.count > 0 {
            UIDesigns.getLastDesignListForUser(User.currentUser().id, type: "favourite", latestDesignId: (arrayDesigns.lastObject as! UIDesigns).designId, completion: { (success, result, error) -> Void in
                if success{
                    let array : NSArray = result as! NSArray as [AnyObject]
                    self.arrayDesigns.addObjectsFromArray(array as [AnyObject])
                    self.loadMoreDesign()
                }
                self.refreshControlDesignList.endRefreshing()
                self.spinner.stopAnimating()
                self.spinner.hidden = true
                self.tableView.reloadData()
            })
        }
        else {
            self.LoadInitialDesign()
        }
        
    }
    
    
    // MARK: TableView
    // MARK:  TableView DataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if arrayDesigns == nil {
            return 0;
        }
        else {
            return arrayDesigns.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("designListCellId") as! DesignListTableViewCell
        
        // Configure the cell...
        
        cell.setDesignDetails(arrayDesigns[indexPath.row] as! UIDesigns)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //        DesignDetailVC
        selectedIndex = indexPath
        let designDetailViewController : DesignDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DesignDetailVC") as! DesignDetailViewController
        designDetailViewController.designDetails = arrayDesigns[indexPath.row] as? UIDesigns
        designDetailViewController.delegate = self
       // self.navigationController?.tabBarController?.navigationController?.pushViewController(designDetailViewController, animated: false)
        self.navigationController?.tabBarController?.navigationController?.presentViewController(designDetailViewController, animated: true, completion: nil)


        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [DesignListTableViewCell] {
            let x = cell.imageViewDesignThumbnail.frame.origin.x
            let w = cell.imageViewDesignThumbnail.bounds.width
            let h = cell.imageViewDesignThumbnail.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 25
            cell.imageViewDesignThumbnail.frame = CGRectMake(x, y, w, h)
        }
    }
    
//    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let currentOffset = scrollView.contentOffset.y;
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//        
//        //NSInteger result = maximumOffset - currentOffset;
//        
//        // Change 10.0 to adjust the distance from bottom
//        if ((maximumOffset - currentOffset <= 10.0) && (arrayDesigns.count > 0) && self.refreshControl?.refreshing == false) {
//            self.spinner.startAnimating()
//            self.spinner.hidden = false
//            self.loadMoreDesign()
//            //[self methodThatAddsDataAndReloadsTableView];
//        }
//    }
    
    //MARK: DesignDetailViewControllerDelegate
    func didChangeFavoriteStatusOfDesign(design: UIDesigns?) {
        if arrayDesigns.containsObject(design!) {
//            if design?.designIsFavorite == "0" {
//                arrayDesigns.removeObject(design!)
//                self.tableView.reloadData()
//            }
        }
    }


}
