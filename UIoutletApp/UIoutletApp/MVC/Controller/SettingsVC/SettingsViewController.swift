//
//  SettingsViewController.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import MessageUI

protocol SettingsViewControllerDelegate {
    func didLogout()
}

class SettingsViewController: UITableViewController, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate {
    
    var delegate: SettingsViewControllerDelegate?
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var imageViewSocialType: UIImageView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var constraintNameLabelHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.blackColor(),
                NSFontAttributeName: UIFont(name: ".HelveticaNeueDeskInterface-Bold", size: 21)!]
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: ".HelveticaNeueDeskInterface-Regular", size: 18)!], forState: UIControlState.Normal)
        
        
        if User.currentUser().userName != nil {
            labelUserName.text = User.currentUser().userName
        }
        
        if (User.currentUser().firstName != nil && User.currentUser().lastName != nil) {
            labelUserName.text = User.currentUser().firstName! + " " + User.currentUser().lastName!
        }
        
        
        self.fixNameLabelHeight()
        
        textFieldEmail.text = User.currentUser().email!
//        if (User.currentUser().image != nil && (User.currentUser().image!.characters.count) != 0 && !(User.currentUser().image!.isEmpty)) {
//            imageViewProfilePicture.sd_setImageWithURL(NSURL(string: User.currentUser().image!))
//        }

        if let profilePic: String = NSUserDefaults.standardUserDefaults().objectForKey("kProfilePic") as? String {
            imageViewProfilePicture.sd_setImageWithURL(NSURL(string: profilePic))
        }
        
        if (User.currentUser().socialType != nil && (User.currentUser().socialType!.characters.count) != 0 && !(User.currentUser().socialType!.isEmpty) && User.currentUser().socialType == "fb") {
            imageViewSocialType.image = UIImage(named: "Facebook_user_label")
        }
        else {
            imageViewSocialType.image = UIImage(named: "Linkedin_user_label")
        }
        
        
        
        if User.currentUser().facebookId == nil && User.currentUser().linkedInId == nil {
            imageViewSocialType.image = UIImage(named: "Uioutlet_user_label")
        }
        
        imageViewProfilePicture.layer.cornerRadius = 49.0
        imageViewProfilePicture.layer.masksToBounds = true
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - FixNameLabelHeight
    func fixNameLabelHeight() {
        self.view.layoutIfNeeded()
        let attributes = [NSFontAttributeName : labelUserName.font]
        labelUserName.lineBreakMode = NSLineBreakMode.ByWordWrapping
        let rect = labelUserName.text!.boundingRectWithSize(CGSizeMake(labelUserName.frame.size.width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil)
        constraintNameLabelHeight.constant = rect.size.height
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.textFieldEmail.resignFirstResponder()
        self.textFieldEmail.userInteractionEnabled = false
        return true
    }
    
    // MARK: TableView
    // MARK:  TableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 3 :
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
            break
            
        default :
            break
        }
    }
    
    
    // MARK: - MailComposer
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["submit@uioutlet.com"])
        //        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        //        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: nil, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - ButtonAction
    
    @IBAction func doneButtonAction(sender: AnyObject) {
        
//        if textFieldEmail.text != User.currentUser().email {
//            
//            if self.isValidEmail(textFieldEmail.text!) {
//                
//                User.currentUser().updateUserMail(textFieldEmail.text, completion: { (success, result, error) -> Void in
//                    
//                    if !success {
//                        UIAlertView(title: "Warning", message: result as? String, delegate: nil, cancelButtonTitle: "OK").show()
//                        self.textFieldEmail.text = User.currentUser().email!
//                        return
//                    }
//                    else {
//                        self.dismissViewControllerAnimated(true, completion: nil)
//                    }
//                    
//                })
//            }
//            else {
//                UIAlertView(title: "Warning", message: "Please enter a valid email address", delegate: nil, cancelButtonTitle: "OK").show()
//                return
//            }
//            
//        }
//        else {
            self.dismissViewControllerAnimated(true, completion: nil)
//        }
        
        
    }
    
    @IBAction func editButtonAction(sender: AnyObject) {
        let editButton = sender as! UIButton
        editButton.selected = !editButton.selected
        
        if editButton.selected == true {
            editButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            self.textFieldEmail.userInteractionEnabled = true
            self.textFieldEmail.becomeFirstResponder()
        }
        else {
            editButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            self.textFieldEmail.userInteractionEnabled = false
            self.textFieldEmail.resignFirstResponder()
            
            if textFieldEmail.text != User.currentUser().email {
                
                if self.isValidEmail(textFieldEmail.text!) {
                    
                    User.currentUser().updateUserMail(textFieldEmail.text, completion: { (success, result, error) -> Void in
                        
                        if !success {
                            
                            if let errorResult = result as? String {
                                
                                if (errorResult == "This email id already registerd.") {
                                    UIAlertView(title: "Sorry!", message: "This email is already in use with another member. Try another email.", delegate: nil, cancelButtonTitle: "OK").show()
                                }
                                else {
                                    UIAlertView(title: "Warning", message: result as? String, delegate: nil, cancelButtonTitle: "OK").show()
                                }
                                
                            
                            self.textFieldEmail.text = User.currentUser().email!
                            }
                            return
                        }
                    })
                }
                else {
                    UIAlertView(title: "Warning", message: "Please enter a valid email address", delegate: nil, cancelButtonTitle: "OK").show()
                    return
                }
                
            }
        }
        
        
    }
    
    @IBAction func logoutButtonAction(sender: AnyObject) {
        UIAlertView(title: "Message", message: "Are you sure you want to logout?", delegate: self, cancelButtonTitle: "Yes", otherButtonTitles: "No").show()
    }
    
    
    // MARK: - ValidateEmail
    func isValidEmail(emailCheck:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(emailCheck)
    }
    
    //MARK: - UIAlertViewDelegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch (buttonIndex){
        case alertView.cancelButtonIndex :
                User.deleteUserDetails()
                self.delegate?.didLogout()
            break
        default :
            break
        }
    }

}
