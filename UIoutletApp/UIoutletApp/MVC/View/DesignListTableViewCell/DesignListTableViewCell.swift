//
//  DesignListTableViewCell.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class DesignListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewDesignThumbnail: UIImageView!
    @IBOutlet weak var imageViewDesignType1: UIImageView!
    @IBOutlet weak var viewThumbnail: UIView!
    @IBOutlet weak var imageViewDesignType2: UIImageView!
    @IBOutlet weak var labelDesignName: UILabel!
    @IBOutlet weak var labelDesignAuthor: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewThumbnail.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelDesignAuthor.text = ""
        labelDesignName.text = ""
        imageViewDesignThumbnail.image = nil
        imageViewDesignType1.image = nil
        imageViewDesignType2.image = nil
    }
    
    func setDesignDetails(designDetails:UIDesigns){
        labelDesignName.text = designDetails.designName
         if ((designDetails.designAuthorName?.characters.count) != nil && !(designDetails.designAuthorName!.isEmpty)) {
            labelDesignAuthor.text = "by " + designDetails.designAuthorName!
        }
        imageViewDesignThumbnail.sd_setImageWithURL(NSURL(string: ImageDomainURL+designDetails.designThumbnail!), placeholderImage: UIImage(named: "placeHolderDesign"))
//        imageViewDesignThumbnail.sd_setImageWithURL(NSURL(string: ImageDomainURL+designDetails.designThumbnail!))
        
        if ((designDetails.designSketchFile?.characters.count) != nil && !(designDetails.designSketchFile!.isEmpty)) {
            imageViewDesignType1.image = UIImage(named: "Sketch_icon")
            if ((designDetails.designPsdFile?.characters.count) != nil && !(designDetails.designPsdFile!.isEmpty)) {
                imageViewDesignType2.image = UIImage(named: "Photoshop_icon")
            }
            else {
                imageViewDesignType2.image = nil
            }
        }
        else {
            if ((designDetails.designPsdFile?.characters.count) != nil && !(designDetails.designPsdFile!.isEmpty)) {
                imageViewDesignType1.image = UIImage(named: "Photoshop_icon")
                imageViewDesignType2.image = nil
            }
            else {
                imageViewDesignType1.image = nil
                imageViewDesignType2.image = nil
            }
        }
    }

}
