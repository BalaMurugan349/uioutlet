//
//  GalleryCollectionViewCell.swift
//  UIoutletApp
//
//  Created by Aswin on 17/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

protocol GalleryCollectionViewCellDelegate {
    func didTapPlayButton(videoName : String)
}

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewGalleryImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playButton: UIButton!
    var delegate: GalleryCollectionViewCellDelegate?
    var galleryItem : String?
    
    func setGalleryImage(galleryImage:String){
        self.galleryItem = galleryImage
        
//        if galleryImage.rangeOfString("png") != nil || galleryImage.rangeOfString("jpg") != nil || galleryImage.rangeOfString("jpeg") != nil {
//            print("yes")
//            self.playButton.hidden = true
//        }
//        else {
//            self.playButton.hidden = false
//        }
        
        imageViewGalleryImage.sd_setImageWithURL(NSURL(string: ImageDomainURL + galleryImage), placeholderImage: UIImage(named: "placeHolder"))
    }
   
    @IBAction func playButtonAction(sender: AnyObject) {
        self.delegate?.didTapPlayButton(self.galleryItem!)
    }
}
