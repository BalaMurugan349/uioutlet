//
//  CustomPresentAnimation.swift
//  UIoutletApp
//
//  Created by Aswin on 20/12/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class CustomPresentAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        let containerView = transitionContext.containerView()
        
        containerView.addSubview(fromViewController.view)
        containerView.addSubview(toViewController.view)
        
        toViewController.view.alpha = 0.0
        
        UIView.animateWithDuration(self.transitionDuration(transitionContext), delay: 0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
            toViewController.view.alpha = 1.0
            
            }) { (finished : Bool) -> Void in
                
                fromViewController.view.removeFromSuperview()
                transitionContext.completeTransition(true)
        }
    }
}
