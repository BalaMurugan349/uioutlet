//
//  CustomNavigationAnimation.swift
//  UIoutletApp
//
//  Created by Aswin on 20/12/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class CustomPushNavigationAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    /*
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 2.0
    }
    
    
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! UITableViewController
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! DesignDetailViewController
        let containerView = transitionContext.containerView()
        
        let indexPath = fromViewController.valueForKey("selectedIndex") as! NSIndexPath
        let cell = fromViewController.tableView.cellForRowAtIndexPath(indexPath) as! DesignListTableViewCell
        
        let cellImageSnapShot = cell.imageViewDesignThumbnail.snapshotViewAfterScreenUpdates(false)
        cellImageSnapShot.frame = (containerView?.convertRect(cell.imageViewDesignThumbnail.frame, fromView: cell.imageViewDesignThumbnail.superview))!
        cell.imageViewDesignThumbnail.hidden = true
        
        
        toViewController.view.frame = transitionContext.finalFrameForViewController(toViewController)
        toViewController.view.alpha = 0.0
        toViewController.headerView.imageView?.hidden = true
        
        containerView?.addSubview(toViewController.view)
        containerView?.addSubview(cellImageSnapShot)
        
        
        UIView .animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
            cellImageSnapShot.frame = toViewController.view.bounds
            }) { (finished : Bool) -> Void in
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    
                    cellImageSnapShot.frame = (toViewController.tableView.tableHeaderView?.frame)!
                    
                    }, completion: { (finished : Bool) -> Void in
                        UIView.animateWithDuration(0.2, animations: { () -> Void in
                            cellImageSnapShot.alpha = 0.0
                            }, completion: { (finished : Bool) -> Void in
                                toViewController.headerView.imageView?.hidden = false
                                cell.imageViewDesignThumbnail.hidden = false
                                cellImageSnapShot.removeFromSuperview()
                                transitionContext.completeTransition(true)
                        })
                        
                })
        }
        
        /*
        
        //DSLFirstViewController *fromViewController = (DSLFirstViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        //DSLSecondViewController *toViewController = (DSLSecondViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
       // UIView *containerView = [transitionContext containerView];
        NSTimeInterval duration = [self transitionDuration:transitionContext];
        
        // Get a snapshot of the thing cell we're transitioning from
     //   DSLThingCell *cell = (DSLThingCell*)[fromViewController.collectionView cellForItemAtIndexPath:[[fromViewController.collectionView indexPathsForSelectedItems] firstObject]];
       // UIView *cellImageSnapshot = [cell.imageView snapshotViewAfterScreenUpdates:NO];
       // cellImageSnapshot.frame = [containerView convertRect:cell.imageView.frame fromView:cell.imageView.superview];
      //  cell.imageView.hidden = YES;
        
        // Setup the initial view states
      //  toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
      //  toViewController.view.alpha = 0;
      //  toViewController.imageView.hidden = YES;
        
     //   [containerView addSubview:toViewController.view];
     //   [containerView addSubview:cellImageSnapshot];
        
        [UIView animateWithDuration:duration animations:^{
        // Fade in the second view controller's view
        toViewController.view.alpha = 1.0;
        
        // Move the cell snapshot so it's over the second view controller's image view
        CGRect frame = [containerView convertRect:toViewController.imageView.frame fromView:toViewController.view];
        cellImageSnapshot.frame = frame;
        } completion:^(BOOL finished) {
        // Clean up
        toViewController.imageView.hidden = NO;
        cell.hidden = NO;
        [cellImageSnapshot removeFromSuperview];
        
        // Declare that we've finished
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
        }];

*/
    }
*/
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        let fromView = fromViewController.view
        let toView = toViewController.view
        
//        let currentScreen = UIImageView(image: self.captureScreen())
        
        
        let containerView = transitionContext.containerView()
        
        containerView.addSubview(fromView)
        containerView.addSubview(toView)
        
        let fromViewFrame = containerView.bounds
        var toViewFrame = fromViewFrame
        toViewFrame.origin.x = CGRectGetWidth(fromViewFrame)
        
        var fromViewFinalFrame = fromViewFrame
        fromViewFinalFrame.origin.x = (fromViewFrame.origin.x) - CGRectGetWidth(fromViewFrame)
        
        fromView.frame = fromViewFrame
        toView.frame = toViewFrame
        
//        toViewController.view.alpha = 0.0
        
        UIView.animateWithDuration(self.transitionDuration(transitionContext), delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            fromView.frame = fromViewFinalFrame
            toView.frame = fromViewFrame
            
            }) { (finished : Bool) -> Void in
                
                fromView!.removeFromSuperview()
                transitionContext.completeTransition(true)
        }
        
        
        /*
        CGRect startFrame = endFrame;
        startFrame.origin.x += CGRectGetWidth(([[transitionContext containerView] bounds]));
        toViewController.view.frame = startFrame;
        
        CGRect endFrameFromView = endFrame;
        endFrameFromView.origin.x -= CGRectGetWidth(([[transitionContext containerView] bounds]));
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext]
            delay:0
            options:UIViewAnimationOptionCurveLinear
            animations:^{
            toViewController.view.frame = endFrame;
            fromViewController.view.frame = endFrameFromView;
            } completion:^(BOOL finished) {
            BOOL didComplete = [transitionContext transitionWasCancelled] == NO;
            [transitionContext completeTransition:didComplete];
            }];
*/
    }
    
    
}
