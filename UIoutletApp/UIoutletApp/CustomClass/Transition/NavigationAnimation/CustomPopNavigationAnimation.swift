//
//  CustomPopNavigationAnimation.swift
//  UIoutletApp
//
//  Created by Aswin on 20/12/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

class CustomPopNavigationAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 2.0
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
    }

}
