//
//  ServiceManager.swift
//  UIoutletApp
//
//  Created by Aswin on 02/10/15.
//  Copyright © 2015 Aswin. All rights reserved.
//

import UIKit

//let DomainURL = "http://45.55.60.74/JasonUX/jasonux/index.php/"

let DomainURL = "http://uioutlet.net/JasonUX/jasonux/index.php/"
let ImageDomainURL = "http://uioutlet.net/JasonUX/jasonux/images/"
let VideoDomainURL = "http://uioutlet.net/JasonUX/jasonux/videos/"


enum ServiceName {
    case LoginService
    case MainDesign
    case FavoriteDeign
    case PurchasedDesign
    case RefreshMainDesign
    case RefreshFavoriteDeign
    case RefreshPurchasedDesign
}

class ServiceManager: NSObject, NSURLConnectionDataDelegate {
    
    var urlData : NSMutableData!
    var domainUrl : String!
    var baseUrl : String!
    typealias completionBlock = (Bool? ,AnyObject? ,NSError?) -> Void
    var finishLoading: completionBlock!
    
    
    override init() {
        super.init()
        //setting main url
        domainUrl = DomainURL;
        baseUrl = domainUrl + "api/"
    }
    
    class func fetchDataFromService (serviceName: String, parameters : NSData?, withCompletion :(success : Bool, result: AnyObject!, error: NSError?)-> Void){
        
        let serviceManager = ServiceManager()
        serviceManager.httpRequestForService(serviceName, parameters: parameters) { (success, result, error) -> Void in
            withCompletion(success: success!, result: result, error: error)
        }
    }
    
    
    func httpRequestForService(serviceName: String, parameters : NSData?, completion :(success : Bool?, result: AnyObject?, error: NSError?)-> Void) {
        finishLoading = completion
        let url = NSURL(string: String("\(baseUrl)\(serviceName)"))
        let request = prepareRequestWithURL(url!, parameters: parameters)
        _ = NSURLConnection(request: request, delegate: self, startImmediately: true)
        
    }
    
    
    func prepareRequestWithURL(url: NSURL, parameters : NSData?) -> NSURLRequest {
        let request = NSMutableURLRequest()
        request.URL = url
       
        if url.absoluteString!.containsString("login/registration") || url.absoluteString!.containsString("login/login?") {
            request.HTTPMethod = "GET"
            return request
        }
        
        request.HTTPMethod = "POST"
        let charset = CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding))
        let contentType = String("multipart/form-data; charset=\(charset); boundary=0xKhTmLbOuNdArY")
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        if(parameters != "")
        {
            request.HTTPBody = parameters
        }
        
        
        
        
        return request
    }
    
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        urlData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        urlData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
//        var error:NSError? = NSError()
        
        var json: AnyObject?
        do {
            json = try NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions())
        } catch _ as NSError {
//            error = error1
            json = nil
        }
        
        finishLoading(true , json, nil)
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        finishLoading(false, nil, error)
    }

    
    
    func getServiceURL(serviceRequest : ServiceName) -> String {
//        switch (serviceRequest)
//        {
//
//        }
        return "hello"
    }
}
